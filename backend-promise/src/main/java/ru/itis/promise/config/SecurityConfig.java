package ru.itis.promise.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.itis.promise.security.CustomUserDetailsService;
import ru.itis.promise.security.JwtAuthenticationEntryPoint;
import ru.itis.promise.security.JwtAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true, // to protect controller/service by @Secured annotation
        jsr250Enabled = true, // enables @RolesAllowed annotation
        prePostEnabled = true // enables @PreAuthorize and @PostAuthorize annotations
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(customUserDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors() // Cross-Origin Resource Sharing
                .and()
                .csrf() // Cross-Site Request Forgery
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedHandler)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/",
                        "/favicon.ico",
                        "/**/*.png",
                        "/**/*.gif",
                        "/**/*.svg",
                        "/**/*.jpg",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js")
                .permitAll()
                .antMatchers("/api/auth/**")
                .permitAll()
                .antMatchers("/api/public")
                .permitAll()
                .antMatchers("/api/private")
                .authenticated()
                .antMatchers("/api/contract_appeal/create")
                .authenticated()
                .antMatchers("/api/tags")
                .permitAll()
                .antMatchers("/api/moderator/**").hasRole("MODERATOR")
                .antMatchers("/ws/**").permitAll()
                .antMatchers("/api/contract_appeal/all").permitAll()
                .antMatchers("/api/contract_appeal/*").permitAll()
                .antMatchers("/api/contract/create").hasRole("MODERATOR")
                .antMatchers("/api/wshello").permitAll()
                .antMatchers("/api/contacts").authenticated()
                .antMatchers("/api/contract/**").permitAll()
                .antMatchers("/api/contract/connect").hasRole("MODERATOR")
                .antMatchers("/api/user/{userId:[\\d+]}").permitAll()
                .antMatchers("/api/user/edit").authenticated()
                .antMatchers("/api/contract-appeals/my").authenticated()
                .antMatchers("/api/contract_appeals/delete/{id:[\\d+]}").authenticated()
                .anyRequest()
                .denyAll();

        // Add our custom JWT security filter
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
