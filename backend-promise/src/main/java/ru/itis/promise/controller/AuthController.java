package ru.itis.promise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.itis.promise.model.User;
import ru.itis.promise.payload.request.LoginRequest;
import ru.itis.promise.payload.request.SignUpRequest;
import ru.itis.promise.payload.response.ApiResponse;
import ru.itis.promise.payload.response.JwtAuthenticationResponse;
import ru.itis.promise.payload.response.LoginResponse;
import ru.itis.promise.service.AuthenticationService;
import ru.itis.promise.validators.SignUpRequestValidator;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthenticationService authService;

    private final SignUpRequestValidator signUpRequestValidator;

    @Autowired
    public AuthController(AuthenticationService authService, SignUpRequestValidator signUpRequestValidator) {
        this.authService = authService;
        this.signUpRequestValidator = signUpRequestValidator;
    }

    @InitBinder("signUpRequest")
    public void initUserFormValidator(WebDataBinder binder) {
        binder.addValidators(signUpRequestValidator);
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        try {
            User user = authService.getUserFromLoginRequest(loginRequest);
            String accessToken = authService.createJwtAccessToken(user);
            String refreshToken = authService.createJwtRefreshToken(user);
            user.setLastVisited(new Date());
            authService.updateRefreshToken(user, refreshToken);
            Long accessExpiresIn = authService.getJwtExpirationTimeInMs();
            return ResponseEntity.ok(new LoginResponse(user.getId(), user.getUsername(), user.getFirstName(), user.getLastName(), accessToken, refreshToken, accessExpiresIn, user.getRoles(), user.getAvatarUrl()));
        } catch (IllegalArgumentException e) {
            // from Spring 5+
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest, BindingResult errors) {
        if (errors.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, errors.getAllErrors().get(0).getDefaultMessage());
        }
        User user = authService.createUser(signUpRequest);
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(user.getUsername()).toUri();

        return ResponseEntity.created(location).body("User created successfully");
    }

    @PostMapping("/refresh")
    public ResponseEntity<?> refreshJwtToken(@RequestBody Map<String, String> payload) throws IOException {
        String refreshToken = payload.get("refreshToken");
        if (!authService.validateToken(refreshToken)) {
            return new ResponseEntity<>(new ApiResponse(false, "Refresh token is expired"),
                    HttpStatus.UNAUTHORIZED);
        }
        User user = authService.getUserFromRefreshToken(refreshToken);
        if (user.getRefreshToken().equals(refreshToken)) {
            String accessToken = authService.createJwtAccessToken(user);
            String newRefreshToken = authService.createJwtRefreshToken(user);
            Long accessExpiresIn = authService.getJwtExpirationTimeInMs();

            user.setLastVisited(new Date());
            authService.updateRefreshToken(user, newRefreshToken);
            return ResponseEntity.ok(new JwtAuthenticationResponse(accessToken, newRefreshToken, accessExpiresIn));
        } else {
            return new ResponseEntity<>(new ApiResponse(false, "Refresh token didn't matched!"),
                    HttpStatus.BAD_REQUEST);
        }
    }
}
