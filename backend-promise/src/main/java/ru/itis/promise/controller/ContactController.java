package ru.itis.promise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.promise.payload.response.ContactResponse;
import ru.itis.promise.service.ContactService;

import java.util.Set;

@RestController
@RequestMapping("/api")
public class ContactController {

    private final ContactService contactService;

    @Autowired
    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping(value = "/contacts")
    public ResponseEntity<?> getContacts(){
        Set<ContactResponse> contactResponseSet = contactService.getCurrentUserContactsDto();
        return ResponseEntity.ok(contactResponseSet);
    }
}
