package ru.itis.promise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.itis.promise.model.ContractAppeal;
import ru.itis.promise.model.User;
import ru.itis.promise.payload.request.ContractAppealRequest;
import ru.itis.promise.payload.response.ContractAppealResponse;
import ru.itis.promise.service.AuthenticationService;
import ru.itis.promise.service.ContractAppealService;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ContractAppealController {

    private final ContractAppealService contractAppealService;

    private final AuthenticationService authService;

    @Autowired
    public ContractAppealController(ContractAppealService contractAppealService, AuthenticationService authService) {
        this.contractAppealService = contractAppealService;
        this.authService = authService;
    }

    @PostMapping(value = "/contract_appeal/create")
    public ResponseEntity<?> createContractAppeal(@RequestBody ContractAppealRequest contractAppealRequest) {
        ContractAppeal contractAppeal = contractAppealService.saveContractAppealRequest(contractAppealRequest);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(contractAppeal.getId()).toUri();

        return ResponseEntity.created(location).body(contractAppeal.getId());
    }

    @GetMapping(value = "/contract_appeal/all")
    public ResponseEntity<?> getAllContractAppeals(@RequestParam(name = "status", required = false) String status) {
        List<ContractAppeal> contractAppealSet = status == null? contractAppealService.getAllContractAppeals() : contractAppealService.getAllContractAppealsByStatus(status);
        List<ContractAppealResponse> response = contractAppealSet.stream().map(ContractAppealResponse::new).collect(Collectors.toList());
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/contract_appeal/{id}")
    public ResponseEntity<?> getContractAppeal(@PathVariable String id) {
        ContractAppeal contractAppeal = contractAppealService.findContractAppealById(new Long(id));
        ContractAppealResponse response = new ContractAppealResponse(contractAppeal);
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/contract-appeals/my")
    public ResponseEntity<?> getContractAppeal() {
        User user = authService.getAuthenticatedUser();
        Set<ContractAppealResponse> responseSet = user.getContractAppealSet().stream()
                .map(ContractAppealResponse::new)
                .collect(Collectors.toSet());

        return ResponseEntity.ok(responseSet);
    }

    @PostMapping(value = "/contract_appeals/delete/{id}")
    public ResponseEntity<?> deleteContractAppeal(@PathVariable Long id) {
        User user = authService.getAuthenticatedUser();
        Optional<ContractAppeal> contractAppeal =  user.getContractAppealSet().stream().filter(o -> o.getId().equals(id)).findAny();
        if (contractAppeal.isPresent()){
            user.getContractAppealSet().remove(contractAppeal.get());
            contractAppealService.deleteById(id);
        }
        return ResponseEntity.ok("Contract Appeal with id: " + id + ", successfully deleted");
    }
}
