package ru.itis.promise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.promise.model.Contract;
import ru.itis.promise.model.User;
import ru.itis.promise.payload.request.DrawUpContractRequest;
import ru.itis.promise.payload.response.ContractResponse;
import ru.itis.promise.service.AuthenticationService;
import ru.itis.promise.service.ContractService;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ContractController {

    @Autowired
    private ContractService contractService;

    @Autowired
    private AuthenticationService authService;

    @PostMapping(value = "/contract/create")
    public ResponseEntity<?> createContract(@RequestBody DrawUpContractRequest drawUpContractRequest) {
        Contract contract = contractService.save(drawUpContractRequest);

        return ResponseEntity.ok(contract.getId());
    }

    @GetMapping(value = "/contract/{contractId}")
    public ResponseEntity<?> getContactById(@PathVariable(value = "contractId") String contractId) {
        ContractResponse contract = contractService.getContractResponse(contractId);

        return ResponseEntity.ok(contract);
    }

    @PostMapping(value = "/contract/connect")
    public ResponseEntity<?> confirmUsersContractConnection(@RequestBody Map<String, String> payload){
        Long contractId = new Long(payload.get("id"));
        contractService.confirmConnection(contractId);
        return new ResponseEntity<>("New contact successfully created!", HttpStatus.OK);
    }

    @GetMapping(value = "/moderator/contracts")
    public ResponseEntity<?> getCreatedContracts() {
        User user = authService.getAuthenticatedUser();
        Set<Contract> contractSet = user.getDrewUpContracts();

        Set<ContractResponse> contractResponses = contractSet.stream()
                .map(ContractResponse::new)
                .collect(Collectors.toSet());

        return ResponseEntity.ok(contractResponses);
    }
}
