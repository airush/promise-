package ru.itis.promise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.promise.service.TagService;

@RestController
@RequestMapping("/api")
public class TagController {

    @Autowired
    private TagService tagService;

    @GetMapping(value = "/tags")
    public ResponseEntity<?> getAllTags() throws JSONException {
        String tagsString = tagService.getTagsStringForChipsAutocomplete();
        return ResponseEntity.ok(tagsString);
    }
}
