package ru.itis.promise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.promise.model.User;
import ru.itis.promise.payload.request.UserEditRequestBody;
import ru.itis.promise.payload.response.UserResponse;
import ru.itis.promise.service.AmazonService;
import ru.itis.promise.service.AuthenticationService;
import ru.itis.promise.service.UserService;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api")
public class UserController {

    private final UserService userService;

    private final AuthenticationService authService;

    @Autowired
    private AmazonService amazonService;

    @Autowired
    public UserController(AuthenticationService authService, UserService userService) {
        this.authService = authService;
        this.userService = userService;
    }

    @GetMapping(value = "/user/{userId}")
    public ResponseEntity<?> getUserProfile(@PathVariable(value = "userId") Long userId) {
        User user = userService.findUserByUserId(userId);

        UserResponse response = UserResponse.builder()
                .userId(userId)
                .username(user.getUsername())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .createdAt(user.getCreatedAt()) // user.createdAt is not Date
                .about(user.getAbout())
                .birthday(user.getBirthday())
                .lastVisited(user.getLastVisited())
                .status(user.getStatus())
                .gender(user.getGender())
                .avatarUrl(user.getAvatarUrl())
                .build();

        return ResponseEntity.ok().body(response);
    }

    @PostMapping(value = "/user/edit", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity editUserProfile(@RequestPart(value = "avatar", required = false) MultipartFile avatar, @RequestPart UserEditRequestBody requestBody) {
        User user = authService.getAuthenticatedUser();
        userService.updateUserProfile(user, requestBody);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (user.getAvatarUrl()!= null){
            amazonService.deleteFileFromS3Bucket(user.getAvatarUrl());
        }

        String avatarUrl = "Null";
        if (avatar != null){
            avatarUrl = amazonService.uploadFile(avatar);
            userService.updateUserAvatar(user, avatarUrl);
        }
        return new ResponseEntity(avatarUrl, HttpStatus.OK);
    }
}
