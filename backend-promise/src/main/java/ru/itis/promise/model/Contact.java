package ru.itis.promise.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "contacts")
@Getter
@Setter
@ToString
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @ManyToMany(mappedBy = "contactSet", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<User> userSet = new HashSet<>();

    public void addUsers(User user1, User user2){
        if (userSet.isEmpty()) {
            userSet.add(user1);
            user1.getContactSet().add(this);
            userSet.add(user2);
            user2.getContactSet().add(this);
        }else {
            System.err.println("Error: UserSet is not empty");
        }
    }

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST},fetch = FetchType.LAZY)
    @JoinColumn(name = "contract_id")
    @ToString.Exclude
    private Contract contract;

    @OneToMany(mappedBy = "contact", cascade = CascadeType.ALL)
    @ToString.Exclude
    private Set<ContactMessage> contactMessageSet = new HashSet<>();

}
