package ru.itis.promise.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "contract_message")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class ContactMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public ContactMessage(User user, Contact contact, String text){
        this.user = user;
        this.contact=contact;
        this.text = text;
        date = new Date();

        user.getContactMessageSet().add(this);
        contact.getContactMessageSet().add(this);
    }
    //author
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST},fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    private User user;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
   @ToString.Exclude
    @JoinColumn(name = "contact_id")
    private Contact contact;

    private String text;

    private Date date;
}
