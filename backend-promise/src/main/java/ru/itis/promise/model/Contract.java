package ru.itis.promise.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@ToString
@Getter
@Setter
@Table(name = "contract_appeal")
@EqualsAndHashCode(exclude = "contractAppealSet")
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @ManyToMany(mappedBy = "contractSet", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<ContractAppeal> contractAppealSet = new HashSet<>();

    public void addContractAppeals(ContractAppeal contractAppeal1, ContractAppeal contractAppeal2){
        if (contractAppealSet.isEmpty()) {
            contractAppealSet.add(contractAppeal1);
            contractAppeal1.getContractSet().add(this);
            contractAppealSet.add(contractAppeal2);
            contractAppeal2.getContractSet().add(this);
        }else {
            System.err.println("Error: contractAppealSet is not empty");
        }
    }


    private String title;

    private Date lastDay;

    private Integer charge;

    private String details;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST},fetch = FetchType.LAZY)
    @JoinColumn(name = "created_by_id")
    @ToString.Exclude
    private User createdBy;

    @OneToMany(mappedBy = "contract", cascade = CascadeType.ALL)
    @ToString.Exclude
    private Set<Contact> contactSet;

    private Date createdAt;

    public Contract() {
        this.createdAt = new Date();
    }
}