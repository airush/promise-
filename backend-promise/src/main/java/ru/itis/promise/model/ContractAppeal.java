package ru.itis.promise.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "contract_appeals")
@ToString
@Getter
@Setter
@EqualsAndHashCode(exclude = {"user", "tagSet"})
@NoArgsConstructor
@AllArgsConstructor
public class ContractAppeal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private Date approxLastDay;

    private Date createdAt;

    private Integer charge;

    @Column(length = 2000)
    private String describe;

    @Enumerated(EnumType.STRING)
    private ContractAppealStatus status;

    public enum ContractAppealStatus{
        UNCHECKED, WAITING, SOLVED
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    private User user;

    @ToString.Exclude
    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable( name = "join_contract_appeal_tag",
            joinColumns = {@JoinColumn(name = "contract_appeal_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "tag_id", referencedColumnName = "id")})
    private Set<Tag> tagSet = new HashSet<>();

    public ContractAppeal(String title, String describe, Date approxLastDay, Date date, Integer charge, Set<Tag> tags) {
        this.title = title;
        this.describe = describe;
        this.approxLastDay = approxLastDay;
        this.charge = charge;
        this.createdAt = date;
        this.tagSet = tags;
        status = ContractAppealStatus.UNCHECKED;
    }

    @ToString.Exclude
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "join_contract_appeal__to_contract",
            joinColumns = {@JoinColumn(name = "contract_appeal_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "contract_id", referencedColumnName = "id")}
    )
    private Set<Contract> contractSet = new HashSet<>();
}
