package ru.itis.promise.model;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_MODERATOR,
    ROLE_USER
}
