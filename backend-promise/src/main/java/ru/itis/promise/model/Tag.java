package ru.itis.promise.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tags")
@ToString
@Getter
@RequiredArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude ="contractAppealSet" )
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotBlank
    private String name;

    @JsonIgnore
    @ToString.Exclude
    @ManyToMany(mappedBy = "tagSet", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<ContractAppeal> contractAppealSet = new HashSet<>();
}