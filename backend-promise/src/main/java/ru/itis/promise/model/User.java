package ru.itis.promise.model;

import lombok.*;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "username"
        }),
        @UniqueConstraint(columnNames = {
                "email"
        })
})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 30)
    @NotNull
    @NotBlank
    private String firstName;

    @Size(max = 50)
    @NotNull
    @NotBlank
    private String lastName;

    @Column(unique = true)
    @Size(min = 3, max = 15)
    @NotNull
    @NotBlank
    private String username;

    @Email
    @NaturalId
    @Size(max = 40)
    @Column(unique = true)
    @NotNull
    @NotBlank
    private String email;

    @NotNull
    @NotBlank
    @Size(max = 100)
    private String password;

    private String status;
    private String about;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private Date birthday;
    private Date createdAt;
    private Date lastVisited;
    private String avatarUrl;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();
    private String refreshToken;

    public User(@Size(max = 30) @NotNull @NotBlank String firstName, @Size(max = 50) @NotNull @NotBlank String lastName, @Size(min = 3, max = 15) @NotNull @NotBlank String username, @Email @Size(max = 40) @NotNull @NotBlank String email, Set<Role> roles, @NotNull @NotBlank @Size(max = 100) String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private Set<ContactMessage> contactMessageSet = new HashSet<>();

    @ToString.Exclude
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ContractAppeal> contractAppealSet = new HashSet<>();

    public void addContractAppeal(ContractAppeal contractAppeal){
        contractAppealSet.add(contractAppeal);
        contractAppeal.setUser(this);
    }

    public void removeConractAppeal(ContractAppeal contractAppeal){
        contractAppealSet.remove(contractAppeal);
        contractAppeal.setUser(null);
    }

    @OneToMany(mappedBy = "createdBy", cascade = CascadeType.ALL)
    @ToString.Exclude
    private Set<Contract> drewUpContracts = new HashSet<>();

    @ToString.Exclude
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "join_user_to_contact",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "contact_id", referencedColumnName = "id")}
    )
    private Set<Contact> contactSet = new HashSet<>();

    public enum Gender{
        MALE, FEMALE
    }
}