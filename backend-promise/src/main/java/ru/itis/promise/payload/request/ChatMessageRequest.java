package ru.itis.promise.payload.request;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Setter
public class ChatMessageRequest {

    private MessageType type;

    private String text;

    private String sender;

    public enum MessageType{
        CHAT,JOIN,LEAVE
    }

    public MessageType getType(){
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
