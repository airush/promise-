package ru.itis.promise.payload.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;

@Getter
@Setter
@ToString
public class ContractAppealRequest {
    private String title;

    private String describe;

    private Integer charge;

    private Date approxLastDay;

    private ArrayList<String> tags;
}
