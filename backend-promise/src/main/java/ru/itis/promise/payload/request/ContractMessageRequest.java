package ru.itis.promise.payload.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ContractMessageRequest {

    private String text;

    private Long sender;

    private Long contractId;
}
