package ru.itis.promise.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class DrawUpContractRequest {

    private String title;

    private String contractDetails;

    private Integer charge;

    private Date date;

    private Long firstContractAppealId;

    private Long secondContractAppealId;

    private Long moderatorId;
}
