package ru.itis.promise.payload.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class UserEditProfileRequest {

    private String gender;

}
