package ru.itis.promise.payload.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.itis.promise.model.User;

import java.util.Date;

@Setter
@Getter
@ToString
public class UserEditRequestBody {
    private String status;
    private String firstName;
    private String lastName;
    private String about;
    private User.Gender gender;
    private Date birthday;
}
