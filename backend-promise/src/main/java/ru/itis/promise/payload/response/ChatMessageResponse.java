package ru.itis.promise.payload.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ChatMessageResponse {
    private String text;

}
