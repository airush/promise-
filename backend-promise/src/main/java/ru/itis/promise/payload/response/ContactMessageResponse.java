package ru.itis.promise.payload.response;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class ContactMessageResponse {

    private Long id;

    private Long userId;

    private String avatarUrl;

    private String text;

    private Date date;
}
