package ru.itis.promise.payload.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class ContactMsgRespWithContractId {
    private Long contactId;

    private Long id;

    private Long userId;

    private String text;

    private String avatarUrl;

    private Date date;
    public ContactMsgRespWithContractId(Long contactId, Long id, Long userId, String avatarUrl, String text, Date date) {
        this.contactId = contactId;
        this.id = id;
        this.userId = userId;
        this.avatarUrl = avatarUrl;
        this.text = text;
        this.date = date;
    }

 }