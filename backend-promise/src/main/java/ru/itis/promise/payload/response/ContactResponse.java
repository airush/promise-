package ru.itis.promise.payload.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@ToString
public class ContactResponse {
    private Long id;

    private String username;

    private String firstName;

    private String secondName;

    private Set<ContactMessageResponse> contactMessageSet = new HashSet<>();

    private Long contractId;

    private String avatarUrl;

    public ContactResponse(Long id, String username, String avatarUrl, String firstName, String secondName, Set<ContactMessageResponse> contactMessageSet, Long contractId) {
        this.id = id;
        this.username = username;
        this.avatarUrl = avatarUrl;
        this.firstName = firstName;
        this.secondName = secondName;
        this.contactMessageSet = contactMessageSet;
        this.contractId  = contractId;
    }
}
