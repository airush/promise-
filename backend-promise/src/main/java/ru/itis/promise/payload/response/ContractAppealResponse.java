package ru.itis.promise.payload.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.itis.promise.model.ContractAppeal;
import ru.itis.promise.model.Tag;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class ContractAppealResponse {
    public ContractAppealResponse(ContractAppeal contractAppeal) {
        this.id = contractAppeal.getId();
        this.title = contractAppeal.getTitle();
        this.approxLastDay = contractAppeal.getApproxLastDay();
        this.createdAt = contractAppeal.getCreatedAt();
        this.charge = contractAppeal.getCharge();
        this.describe = contractAppeal.getDescribe();
        this.status = contractAppeal.getStatus();
        this.userId = contractAppeal.getUser().getId();
        this.tagSet = contractAppeal.getTagSet();
        this.authorUsername = contractAppeal.getUser().getUsername();
        this.avatarUrl = contractAppeal.getUser().getAvatarUrl();
    }

    private Long id;
    private String title;
    private Date approxLastDay;
    private Date createdAt;
    private Integer charge;
    private String describe;
    @Enumerated(EnumType.STRING)
    private ContractAppeal.ContractAppealStatus status;
    private Long userId;
    private Set<Tag> tagSet;
    private String authorUsername;
    private String avatarUrl;
}
