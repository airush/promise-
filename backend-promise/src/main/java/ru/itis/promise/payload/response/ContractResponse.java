package ru.itis.promise.payload.response;

import lombok.*;
import ru.itis.promise.model.Contract;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
public class ContractResponse {
    private Long id;
    private String title;
    private Date createdAt;
    private String details;
    private Long moderatorId;
    private String moderatorUsername;
    /** This two need to be set directly*/
    private ContractAppeal firstContractAppeal;
    private ContractAppeal secondContractAppeal;

    public ContractResponse(Long id, String title, Date createdAt, String details, Long moderatorId, String moderatorUsername) {
        this.id = id;
        this.title = title;
        this.createdAt = createdAt;
        this.details = details;
        this.moderatorId = moderatorId;
        this.moderatorUsername = moderatorUsername;
    }

    public ContractResponse(Contract contract){
        this.id = contract.getId();
        this.title = contract.getTitle();
        this.createdAt = contract.getCreatedAt();
        this.details = contract.getDetails();
        this.moderatorId = contract.getCreatedBy().getId();
        this.moderatorUsername = contract.getCreatedBy().getUsername();
        List<ContractAppeal> contractAppealList =  contract.getContractAppealSet().stream()
                .map(ContractAppeal::new)
                .collect(Collectors.toList());
        this.firstContractAppeal = contractAppealList.get(0);
        this.secondContractAppeal = contractAppealList.get(1);
    }

    @Getter
    @Setter
    public class ContractAppeal{
        private Long id;
        private Long authorId;
        private String authorUsername;
        private String title;
        private Date createdAt;
        private Date approxLastDay;
        private Integer charge;
        private String description;
        private String authorAvatarUrl;

        public ContractAppeal(Long id, Long authorId, String authorUsername, String authorAvatarUrl, String title, Date createdAt, Date approxLastDay, Integer charge, String description) {
            this.id = id;
            this.authorId = authorId;
            this.authorUsername = authorUsername;
            this.title = title;
            this.createdAt = createdAt;
            this.approxLastDay = approxLastDay;
            this.charge = charge;
            this.description = description;
            this.authorAvatarUrl = authorAvatarUrl;
        }

        public ContractAppeal(ru.itis.promise.model.ContractAppeal contractAppeal){
            this.id = contractAppeal.getId();
            this.authorId = contractAppeal.getUser().getId();
            this.authorUsername = contractAppeal.getUser().getUsername();
            this.authorAvatarUrl = contractAppeal.getUser().getAvatarUrl();
            this.title = contractAppeal.getTitle();
            this.createdAt = contractAppeal.getCreatedAt();
            this.approxLastDay = contractAppeal.getApproxLastDay();
            this.charge = contractAppeal.getCharge();
            this.description = contractAppeal.getDescribe();
        }
    }
}
