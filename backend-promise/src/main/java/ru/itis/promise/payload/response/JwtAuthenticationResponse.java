package ru.itis.promise.payload.response;

import lombok.Getter;


@Getter
public class JwtAuthenticationResponse {

    private String accessToken;

    private String refreshToken;

    private Long expiresIn;

    public JwtAuthenticationResponse(String accessToken, String refreshToken, Long expiresIn) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expiresIn = expiresIn;
    }
}