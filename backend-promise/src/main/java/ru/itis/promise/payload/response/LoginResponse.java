package ru.itis.promise.payload.response;

import lombok.Getter;
import ru.itis.promise.model.Role;

import java.util.Set;

@Getter
public class LoginResponse {

    private Long userId;

    private String username;

    private String firstName;

    private String lastName;

    private String accessToken;

    private String refreshToken;

    private Long expiresIn;

    private Set<Role> roles;

    private String avatarUrl;

    public LoginResponse(Long userId, String username, String firstName, String lastName, String accessToken, String refreshToken, Long expiresIn, Set<Role> roles, String avatarUrl) {
        this.userId = userId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expiresIn = expiresIn;
        this.roles = roles;
        this.avatarUrl = avatarUrl;
    }
}
