package ru.itis.promise.payload.response;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import ru.itis.promise.model.User;

import java.util.Date;

@Builder
@ToString
@Getter
public class UserResponse {

    private Long userId;

    private String username;

    private Date birthday;

    private String firstName;

    private String lastName;

    private Date createdAt;

    private Date lastVisited;

    private String about;

    private String status;

    private User.Gender gender;

    private String avatarUrl;
}
