package ru.itis.promise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.promise.model.ContractAppeal;

import java.util.List;

public interface ContractAppealRepository extends JpaRepository<ContractAppeal, Long> {

    @Query("SELECT cAp FROM  ContractAppeal cAp WHERE cAp.status = :status")
    List<ContractAppeal> findAllByStatus(@Param("status") ContractAppeal.ContractAppealStatus status);
}
