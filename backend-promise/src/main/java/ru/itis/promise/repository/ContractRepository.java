package ru.itis.promise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.promise.model.Contract;

public interface ContractRepository extends JpaRepository<Contract, Long> {
}
