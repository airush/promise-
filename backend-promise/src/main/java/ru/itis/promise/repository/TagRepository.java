package ru.itis.promise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.promise.model.Tag;

public interface TagRepository extends JpaRepository<Tag, Long> {
}
