package ru.itis.promise.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("jwt")
@Getter
@Setter
public class JwtProperties {
    private String secretKey;
    private Long accessTokenExpirationInMs;
    private Long refreshTokenExpirationInMs;
    private String header = "Authorization";
    private String prefix = "Bearer ";
    private String tokenType = "JWT";
}
