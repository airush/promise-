package ru.itis.promise.security;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ru.itis.promise.model.User;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class JwtTokenProvider {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Autowired
    private JwtProperties jwtProps;

    public UserPrincipal getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtProps.getSecretKey())
                .parseClaimsJws(token)
                .getBody();
        Long id = Long.parseLong(claims.getSubject());
        String name = (String) claims.get("nam");
        String lastName = (String) claims.get("sur");
        String username = (String) claims.get("usr");
        List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList((String) claims.get("rol"));
        return new UserPrincipal(id, name, lastName, username, null, null, authorities);
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtProps.getSecretKey()).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
        }
        return false;
    }

    public String getJwtAccessTokenFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader(jwtProps.getHeader());
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(jwtProps.getPrefix())) {
            return bearerToken.substring(7);
        }
        return null;
    }
    /** If returns -1 means Refresh token expired */
    public Long getUserIdFromRefreshToken(String refreshToken) throws IOException {
        boolean valid = validateToken(refreshToken);
        Claims claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(jwtProps.getSecretKey())
                    .parseClaimsJws(refreshToken)
                    .getBody();
            System.out.println(claims);

            return new Long(claims.getSubject());
        } catch (ExpiredJwtException e){
            return -1L;
        }
    }

    public String generateAccessToken(User user) {
        Date now = new Date();
        String rolesSeparatedByComma = user.getRoles().stream()
                .map(role -> role.getName().name())
                .collect(Collectors.joining(","));
        String name = user.getFirstName();
        String username = user.getUsername();

        return Jwts.builder()
                .setSubject(Long.toString(user.getId()))
                .setIssuedAt(now)
                .setExpiration(new Date(now.getTime() + jwtProps.getAccessTokenExpirationInMs()))
                .signWith(SignatureAlgorithm.HS512, jwtProps.getSecretKey())
                .claim("rol", rolesSeparatedByComma)
                .claim("nam", name)
                .claim("sur", user.getLastName())
                .claim("usr", username)
                .compact();
    }

    public String generateRefreshToken(User user) {
        Date now = new Date();
        return Jwts.builder()
                .setSubject(Long.toString(user.getId()))
                .setExpiration(new Date(now.getTime() + jwtProps.getRefreshTokenExpirationInMs()))
                .signWith(SignatureAlgorithm.HS512, jwtProps.getSecretKey())
                .compact();
    }
}
