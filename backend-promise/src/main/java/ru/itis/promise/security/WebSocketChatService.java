package ru.itis.promise.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.promise.model.ChatMessage;
import ru.itis.promise.payload.request.ChatMessageRequest;
import ru.itis.promise.repository.ChatMessageRepository;

import java.util.Date;

@Service
public class WebSocketChatService {

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    public ChatMessage save(ChatMessageRequest chatMessageRequest) {
        ChatMessage message = new ChatMessage();
        message.setDate(new Date());
        message.setText(chatMessageRequest.getText());

        return chatMessageRepository.save(message);
    }
}
