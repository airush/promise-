package ru.itis.promise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.promise.exception.AppException;
import ru.itis.promise.exception.UserNotFoundException;
import ru.itis.promise.model.Role;
import ru.itis.promise.model.RoleName;
import ru.itis.promise.model.User;
import ru.itis.promise.payload.request.LoginRequest;
import ru.itis.promise.payload.request.SignUpRequest;
import ru.itis.promise.repository.RoleRepository;
import ru.itis.promise.security.JwtProperties;
import ru.itis.promise.security.JwtTokenProvider;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;

@Service
public class AuthenticationService {
    private final UserService userService;

    private final JwtTokenProvider jwtTokenProvider;

    private final JwtProperties jwtProperties;

    private final PasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;

    @Autowired
    public AuthenticationService(UserService userService, JwtTokenProvider jwtTokenProvider, JwtProperties jwtProperties, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userService = userService;
        this.jwtTokenProvider = jwtTokenProvider;
        this.jwtProperties = jwtProperties;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    public User getUserFromLoginRequest(LoginRequest loginRequest) {
        User user = null;
        try {
            user = userService.findUserByUsernameOrEmail(loginRequest.getUsernameOrEmail());
        } catch (UserNotFoundException e) {
            throw new IllegalArgumentException("Login or password is incorrect");
        }
        if (!passwordEncoder.matches(loginRequest.getPassword(), user.getPassword()))
            throw new IllegalArgumentException("Login or password is incorrect");
        return user;
    }

    public String createJwtAccessToken(User user) {
        return jwtTokenProvider.generateAccessToken(user);
    }

    public String createJwtRefreshToken(User user) {
        return jwtTokenProvider.generateRefreshToken(user);
    }

    public Long getJwtExpirationTimeInMs() {
        return new Date().getTime() + jwtProperties.getAccessTokenExpirationInMs();
    }

    public User createUser(SignUpRequest signUpRequest) {

        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));
        String password = passwordEncoder.encode(signUpRequest.getPassword());

        User user = new User(signUpRequest.getFirstName(), signUpRequest.getLastName(),signUpRequest.getUsername(),
                signUpRequest.getEmail(), Collections.singleton(userRole), password);

        return userService.save(user);
    }

    public User getUserFromRefreshToken(String refreshToken) throws IOException {
        Long userId = jwtTokenProvider.getUserIdFromRefreshToken(refreshToken);
        User user = userService.findUserByUserId(userId);
        System.out.println(user);
        return user;
    }

    public void updateRefreshToken(User user, String refreshToken) {
        user.setRefreshToken(refreshToken);
        userService.save(user);
    }

    // TODO Get user of authentication
    public User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userService.findByUsername(userDetails.getUsername()).get();
    }

    public boolean validateToken(String token){
        return jwtTokenProvider.validateToken(token);
    }

    public void logout(String userId) {
        System.out.println(userId + "logged out successfully");
    }
}
