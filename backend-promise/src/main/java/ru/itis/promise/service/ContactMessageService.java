package ru.itis.promise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.promise.model.Contact;
import ru.itis.promise.model.ContactMessage;
import ru.itis.promise.model.User;
import ru.itis.promise.repository.ContactMessageRepository;

@Service
public class ContactMessageService {

    private final UserService userService;

    private final ContractService contractService;

    private final ContactMessageRepository contactMessageRepository;

    @Autowired
    public ContactMessageService(ContactMessageRepository contactMessageRepository, UserService userService, ContractService contractService) {
        this.contactMessageRepository = contactMessageRepository;
        this.userService = userService;
        this.contractService = contractService;
    }

    public ContactMessage save(String username, Contact contact, String text) {
        User user = userService.findByUsername(username).get();
        ContactMessage contactMessage = new ContactMessage(user, contact, text);
        return contactMessageRepository.save(contactMessage);
    }
}
