package ru.itis.promise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.promise.model.Contact;
import ru.itis.promise.model.Contract;
import ru.itis.promise.model.ContractAppeal;
import ru.itis.promise.model.User;
import ru.itis.promise.payload.response.ContactMessageResponse;
import ru.itis.promise.payload.response.ContactResponse;
import ru.itis.promise.repository.ContactRepository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ContactService {

    private final ContactRepository contactRepository;

    private final AuthenticationService authService;

    private final UserService userService;

    @Autowired
    public ContactService(ContactRepository contactRepository, AuthenticationService authenticationService, UserService userService) {
        this.contactRepository = contactRepository;
        this.authService = authenticationService;
        this.userService = userService;
    }

    /**
     * Creates three Contact connection between u1-moder, u2-moder
     */
    public void createUserConnectionWithModerator(Contract contract) {
        List<User> userList = contract.getContractAppealSet()
                .stream()
                .map(ContractAppeal::getUser)
                .collect(Collectors.toList());
        User first = userList.get(0);
        User second = userList.get(1);
        User moderator = contract.getCreatedBy();

//        constructAndSaveContact(contract, first, second);
        Contact contact1 = constructAndSaveContact(contract, moderator, first);
        Contact contact2 = constructAndSaveContact(contract, moderator, second);
    }

    public Contact constructAndSaveContact(Contract contract, User user1, User user2) {
        Contact contact = new Contact();
        contact.addUsers(user1, user2);
        contact.setContract(contract);
        return contactRepository.save(contact);
    }

    public Set<ContactResponse> getCurrentUserContactsDto() {
        final User authUser = authService.getAuthenticatedUser();

        return authUser.getContactSet().stream()
                .flatMap(contact -> contact.getUserSet().stream()
                        .filter(user -> !user.getId().equals(authUser.getId()))
                        .map(user1 -> new ContactResponse(contact.getId(), user1.getUsername(), user1.getAvatarUrl(), user1.getFirstName(), user1.getLastName(), contact.getContactMessageSet().stream()
                                .map(message -> new ContactMessageResponse(message.getId(), message.getUser().getId(), message.getUser().getAvatarUrl(), message.getText(), message.getDate()))
                                .collect(Collectors.toSet()), contact.getContract().getId()
                        ))
                )
                .collect(Collectors.toSet());
    }

    @Transactional
    public Contact getCommonContact(String name, String partnerUsername) {
        User user1 = userService.findByUsername(name).get();
        User user2 = userService.findByUsername(partnerUsername).get();

        Contact res = user1.getContactSet().stream()
                .filter(contact -> contact.getUserSet().contains(user2))
                .findFirst().get();
        return res;
    }
}
