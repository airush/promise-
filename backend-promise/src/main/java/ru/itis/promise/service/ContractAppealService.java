package ru.itis.promise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.promise.model.ContractAppeal;
import ru.itis.promise.model.Tag;
import ru.itis.promise.model.User;
import ru.itis.promise.payload.request.ContractAppealRequest;
import ru.itis.promise.repository.ContractAppealRepository;
import ru.itis.promise.repository.TagRepository;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ContractAppealService {

    private final AuthenticationService authenticationService;

    private final ContractAppealRepository contractAppealRepository;

    private final TagRepository tagRepository;

    @Autowired
    public ContractAppealService(AuthenticationService authenticationService, ContractAppealRepository contractAppealRepository, TagRepository tagRepository) {
        this.authenticationService = authenticationService;
        this.contractAppealRepository = contractAppealRepository;
        this.tagRepository = tagRepository;
    }

    public ContractAppeal saveContractAppealRequest(ContractAppealRequest contractReq) {
        User user = authenticationService.getAuthenticatedUser();

        // It has to be refactored
        List<String> reqTags = contractReq.getTags();
        Set<Tag> allTags = new HashSet<>(tagRepository.findAll());
        Set<Tag> resultTags = allTags.stream()
                .filter(tag -> reqTags.stream()
                        .anyMatch(o -> o.equals( tag.getName()))
                )
                .collect(Collectors.toSet());
        System.err.println("ERRR: "+ contractReq.getApproxLastDay());
        ContractAppeal contractAppeal = new ContractAppeal(contractReq.getTitle(), contractReq.getDescribe(), contractReq.getApproxLastDay(), new Date(), contractReq.getCharge(), resultTags);
        user.addContractAppeal(contractAppeal);
        contractAppealRepository.save(contractAppeal);

        return contractAppeal;
    }

    public List<ContractAppeal> getAllContractAppeals() {
        return contractAppealRepository.findAll();
    }

    public ContractAppeal findContractAppealById(Long id) {
        return contractAppealRepository.getOne(id);
    }

    public void deleteById(Long id) {
        contractAppealRepository.deleteById(id);
    }

    @Transactional
    public void updateStatusToSolved(Set<ContractAppeal> contractAppealSet) {
        List<ContractAppeal> contractAppealList = contractAppealSet.stream()
                .peek(contractAppeal -> contractAppeal.setStatus(ContractAppeal.ContractAppealStatus.SOLVED))
                .collect(Collectors.toList());

        contractAppealRepository.saveAll(contractAppealList);
    }


    public List<ContractAppeal> getAllContractAppealsByStatus(String status) {
      return contractAppealRepository.findAllByStatus(ContractAppeal.ContractAppealStatus.valueOf(status));
    }
}
