package ru.itis.promise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.promise.model.*;
import ru.itis.promise.payload.request.DrawUpContractRequest;
import ru.itis.promise.payload.response.ContactMessageResponse;
import ru.itis.promise.payload.response.ContactResponse;
import ru.itis.promise.payload.response.ContractResponse;
import ru.itis.promise.repository.ContractRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ContractService {

    private final ContractRepository contractRepository;

    private final ContractAppealService contractAppealService;

    private final ContactService contactService;

    private final UserService userService;

    private final SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public ContractService(ContractRepository contractRepository, ContractAppealService contractAppealService, ContactService contactService, UserService userService, SimpMessagingTemplate simpMessagingTemplate) {
        this.contractRepository = contractRepository;
        this.contractAppealService = contractAppealService;
        this.contactService = contactService;
        this.userService = userService;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public Contract findById(Long contractId) {
        return contractRepository.findById(contractId).get();
    }

    public Contract save(DrawUpContractRequest reqDto) {
        Contract contract = new Contract();
        contract.setTitle(reqDto.getTitle());
        contract.setCharge(reqDto.getCharge());
        contract.setDetails(reqDto.getContractDetails());
        contract.setLastDay(reqDto.getDate());
        contract.setCreatedBy(userService.findUserByUserId(reqDto.getModeratorId()));

        ContractAppeal firstContractAppeal = contractAppealService.findContractAppealById(reqDto.getFirstContractAppealId());
        ContractAppeal secondContractAppeal = contractAppealService.findContractAppealById(reqDto.getSecondContractAppealId());
        firstContractAppeal.setStatus(ContractAppeal.ContractAppealStatus.WAITING);
        secondContractAppeal.setStatus(ContractAppeal.ContractAppealStatus.WAITING);

        contract.addContractAppeals(firstContractAppeal, secondContractAppeal);

        contract = contractRepository.save(contract);
        // -> creates 2 Contacts
        contactService.createUserConnectionWithModerator(contract);
        return contract;
    }

    @Transactional
    public ContractResponse getContractResponse(String contractId) {
        Contract c = findById(new Long(contractId));
        User moder = c.getCreatedBy();
        List<ContractAppeal> contractAppeals = new ArrayList<>(c.getContractAppealSet());
        ContractAppeal first = contractAppeals.get(0);
        ContractAppeal second = contractAppeals.get(1);

        ContractResponse resp = new ContractResponse(c.getId(),c.getTitle(), c.getCreatedAt(), c.getDetails(), moder.getId(), moder.getUsername());
        ContractResponse.ContractAppeal firstContractAppealResp = resp.new ContractAppeal(first.getId(), first.getUser().getId(), first.getUser().getUsername(), first.getUser().getAvatarUrl(), first.getTitle(), first.getCreatedAt(), first.getApproxLastDay(), first.getCharge(), first.getDescribe());
        ContractResponse.ContractAppeal secondContractAppealResp = resp.new ContractAppeal(second.getId(), second.getUser().getId(), second.getUser().getUsername(), second.getUser().getAvatarUrl(), second.getTitle(), second.getCreatedAt(), second.getApproxLastDay(), second.getCharge(), second.getDescribe());
        resp.setFirstContractAppeal(firstContractAppealResp);
        resp.setSecondContractAppeal(secondContractAppealResp);
        return resp;
    }

    public void confirmConnection(Long contractId) {
        Contract contract = findById(contractId);
        Set<ContractAppeal> contractAppealSet = contract.getContractAppealSet();

        // Creating Contact
        List<User> userList = contractAppealSet
                .stream()
                .map(ContractAppeal::getUser)
                .collect(Collectors.toList());
        User first = userList.get(0);
        User second = userList.get(1);
        Contact c = contactService.constructAndSaveContact(contract, first, second);

        // Выставить всем статус SOLVED, чтобы больше нельзя было создавать контракты с их помощью
        contractAppealService.updateStatusToSolved(contractAppealSet);

        // Sending messages to Users by webSockets
        Set<ContactMessageResponse> contactMessageSet = new HashSet<>();
        ContactResponse contactResponse = new ContactResponse(c.getId(), first.getUsername(), first.getAvatarUrl(), first.getFirstName(), first.getLastName(), contactMessageSet, contractId);
        ContactResponse contactResponse2 = new ContactResponse(c.getId(), second.getUsername(), second.getAvatarUrl(), second.getFirstName(), second.getLastName(), contactMessageSet, contractId);
        Map<String, Object> resp1 = new HashMap<>();
        resp1.put("type", "NEW_CONTACT");
        resp1.put("contact", contactResponse2);

        Map<String, Object> resp2 = new HashMap<>();
        resp2.put("type", "NEW_CONTACT");
        resp2.put("contact", contactResponse);
        simpMessagingTemplate.convertAndSendToUser(first.getUsername(), "/queue/reply", resp1);
        simpMessagingTemplate.convertAndSendToUser(second.getUsername(), "/queue/reply",resp2);
    }
}
