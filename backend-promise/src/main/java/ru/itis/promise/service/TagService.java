package ru.itis.promise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import ru.itis.promise.model.Tag;
import ru.itis.promise.repository.TagRepository;

import java.util.List;

@Service
public class TagService {

    @Autowired
    private TagRepository tagRepository;

    public String getTagsStringForChipsAutocomplete() throws JSONException {
        List<Tag> tags = tagRepository.findAll();
        // Manually creating JSON object for autocomplere. Looking bad solution
        JSONObject json = new JSONObject();
        for (int i = 0; i < tags.size(); i++) {
            json.put(tags.get(i).getName(), JSONObject.NULL);
        }
        return json.toString();
    }
}
