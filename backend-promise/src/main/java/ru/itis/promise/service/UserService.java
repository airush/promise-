package ru.itis.promise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.promise.exception.UserNotFoundException;
import ru.itis.promise.model.User;
import ru.itis.promise.payload.request.UserEditRequestBody;
import ru.itis.promise.repository.UserRepository;

import java.util.Date;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findUserByUsernameOrEmail(String usernameOrEmail) throws UserNotFoundException {
        Optional<User> user;
        user = usernameOrEmail.contains("@") ? userRepository.findByEmail(usernameOrEmail) : userRepository.findByUsername(usernameOrEmail);
        if (user.isPresent()) {
            return user.get();
        }
        throw new UserNotFoundException("User not found by \"" + usernameOrEmail + "\"");
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public boolean userExistsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    public boolean userExistsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public User findUserByUserId(Long userId) {
        User user = userRepository.getOne(userId);
        User user1 = userRepository.findById(userId).get();
        return userRepository.getOne(userId);
    }

    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public void updateUserProfile(User user, UserEditRequestBody reqBody) {
        String status = reqBody.getStatus();
        if (status!= null){
            user.setStatus(status);
        }
        String firstName = reqBody.getFirstName();
        if (firstName!= null){
            user.setFirstName(firstName);
        }
        String lastName = reqBody.getLastName();
        if (lastName!= null){
            user.setLastName(lastName);
        }
        String about = reqBody.getAbout();
        if (about!= null){
            user.setAbout(about);
        }
        Date birthday = reqBody.getBirthday();
        if (birthday!= null){
            user.setBirthday(birthday);
        }
        User.Gender gender = reqBody.getGender();
        if (gender!= null){
            user.setGender(gender);
        }
        user.setLastVisited(new Date());
        userRepository.save(user);
    }

    public void updateUserAvatar(User user, String avatarUrl) {
        user.setAvatarUrl(avatarUrl);
        userRepository.save(user);
    }
}
