package ru.itis.promise.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.itis.promise.model.User;
import ru.itis.promise.payload.request.SignUpRequest;
import ru.itis.promise.service.UserService;

import java.util.Optional;

@Component
public class SignUpRequestValidator implements Validator {

    private final UserService userService;

    @Autowired
    public SignUpRequestValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return SignUpRequest.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        SignUpRequest signUpForm = (SignUpRequest) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "empty.username", "Field \"username\" can not be empty!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "empty.email", "Field \"email\" can not be empty!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "empty.password", "Field \"password\" can not be empty!");

        /** Если нет ошибок то проверяем на наличие уже существующих пользователей в БД. Чтобы не делать запросы в бд каждый раз при валидации. */
        if (!errors.hasErrors()) {
            Optional<User> userByEmail = userService.findByEmail(signUpForm.getEmail());
            Optional<User> userByUsername = userService.findByUsername(signUpForm.getUsername());

            if (userByUsername.isPresent()) {
                errors.reject("bad.username", "This username has already been taken. Please pick another username");
            }
//            if (!Pattern.matches("[a-zA-Z]{3,15}" , signUpForm.getUsername())) {
//                errors.reject("bad.username", "Incorrect username");
//            }
//            if (!Pattern.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$" , form.getEmail())) {
//                errors.reject("bad.email", "Incorrect email address");
//            }
            if (userByEmail.isPresent()) {
                errors.reject("bad.email", "This email has already been taken. Please pick another email");
            }
            if (!signUpForm.getPassword().equals(signUpForm.getPasswordRepeat())) {
                errors.reject("bad.password", "Password are not the same");
            }
        }
    }
}
