package ru.itis.promise.webSocketControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import ru.itis.promise.model.ChatMessage;
import ru.itis.promise.payload.request.ChatMessageRequest;
import ru.itis.promise.security.WebSocketChatService;

@Controller
public class ChatController {

    @Autowired
    private WebSocketChatService chatService;

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessageRequest chatMessageRequest) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        //UserPrincipal user = (UserPrincipal) auth.getPrincipal();
        ChatMessage message = chatService.save(chatMessageRequest);
        return message;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public ChatMessageRequest addUser(@Payload ChatMessageRequest chatMessageRequest, SimpMessageHeaderAccessor headerAccessor){
        try{
            // Add username in web socket session
            headerAccessor.getSessionAttributes().put("username", chatMessageRequest.getSender());

            System.out.println("NEW USER ADDED TO CHAT");
        }catch (NullPointerException e){
            System.err.println("Username is empty!!!");
        }
        return chatMessageRequest;
    }
}
