package ru.itis.promise.webSocketControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.promise.model.Contact;
import ru.itis.promise.model.ContactMessage;
import ru.itis.promise.model.User;
import ru.itis.promise.payload.response.ContactMsgRespWithContractId;
import ru.itis.promise.service.ContactMessageService;
import ru.itis.promise.service.ContactService;
import ru.itis.promise.service.UserService;

import java.security.Principal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
public class UserChatController {

    private final SimpMessagingTemplate simpMessagingTemplate;

    private final ContactService contactService;

    private final ContactMessageService contactMessageService;

    private final UserService userService;

    @Autowired
    public UserChatController(ContactService contactService, SimpMessagingTemplate simpMessagingTemplate, ContactMessageService contactMessageService, UserService userService) {
        this.contactService = contactService;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.contactMessageService = contactMessageService;
        this.userService = userService;
    }

    @MessageMapping("/send")
    @Transactional
    public void sendMessage(MessageHeaders messageHeaders, @Header(name = "simpSessionId") String sessionId, SimpMessageHeaderAccessor sha, Principal principal, Map message) {
        Principal user = sha.getUser();
        String partnerUsername = message.get("to").toString();
        User partnerUser = userService.findByUsername(partnerUsername).get();
        User me = userService.findByUsername(user.getName()).get();
        Contact contact = contactService.getCommonContact(principal.getName(), partnerUsername);
        LinkedHashMap textWrapper = (LinkedHashMap) message.get("message");
        if (contact != null) {
            ContactMessage contactMsg = contactMessageService.save(principal.getName(), contact, textWrapper.get("text").toString());
            ContactMsgRespWithContractId replyMsgToMe = new ContactMsgRespWithContractId(contact.getId(), contactMsg.getId(), contactMsg.getUser().getId(), contactMsg.getUser().getAvatarUrl(),contactMsg.getText(), contactMsg.getDate());
            ContactMsgRespWithContractId replyMsgToPartner = new ContactMsgRespWithContractId(contact.getId(), contactMsg.getId(), contactMsg.getUser().getId(),contactMsg.getUser().getAvatarUrl(), contactMsg.getText(), contactMsg.getDate());

            Map<String, Object> resp1 = new HashMap<>();
            resp1.put("type", "NEW_MESSAGE");
            resp1.put("contact", replyMsgToMe);

            Map<String, Object> resp2 = new HashMap<>();
            resp2.put("type", "NEW_MESSAGE");
            resp2.put("contact", replyMsgToPartner);

            simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/queue/reply", resp1);
            simpMessagingTemplate.convertAndSendToUser(message.get("to").toString(), "/queue/reply", resp2);
        }
    }
}
