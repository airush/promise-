package ru.itis.promise.webSocketControllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import ru.itis.promise.payload.request.ChatMessageRequest;

@Component
public class WebSocketEventListener {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

    @Autowired
    private SimpMessageSendingOperations messageTemplate;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event){
        logger.info("Новое соединение!");
        System.out.println(event);
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event){
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

        String username = (String) headerAccessor.getSessionAttributes().get("username");

        if (username!= null){
            logger.info("User disconnected: " + username);

            ChatMessageRequest chatMessageRequest = new ChatMessageRequest();

            chatMessageRequest.setType(ChatMessageRequest.MessageType.LEAVE);
            chatMessageRequest.setSender(username);

            messageTemplate.convertAndSend("/topic/public", chatMessageRequest);
        }
    }

}
