INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');
INSERT INTO roles(name) VALUES('ROLE_MODERATOR');

-- Create user login: "qwerty", password: "qwerty"
INSERT INTO users(id, created_at, last_visited, email, first_name, password, username, last_name) VALUES (nextval('users_id_seq'), now(),now(), 'aliveairush@gmail.com', 'Ivan','$2a$10$7VozJ43ei.ETCl0ZdyXHIuUGQrQN9C2z6S2POSQDZeNq.gkqH/FNa','qwerty', 'Ivanov');
INSERT INTO user_roles(user_id, role_id) VALUES (currval('users_id_seq'), 1);
-- Create user login: "qwerty1", password: "qwerty"
INSERT INTO users(id, created_at, last_visited, email, first_name, password, username, last_name) VALUES (nextval('users_id_seq'), now(),now(), 'aliveairush1@gmail.com', 'Petr','$2a$10$7VozJ43ei.ETCl0ZdyXHIuUGQrQN9C2z6S2POSQDZeNq.gkqH/FNa','qwerty1', 'Petrov');
INSERT INTO user_roles(user_id, role_id) VALUES (currval('users_id_seq'), 1);

-- Create moderator login: "moderator", password: "qwerty"
INSERT INTO users(id, created_at, last_visited, email, first_name, password, username, last_name) VALUES (nextval('users_id_seq'), now(),now(), 'moderator@gmail.com', 'Dart','$2a$10$7VozJ43ei.ETCl0ZdyXHIuUGQrQN9C2z6S2POSQDZeNq.gkqH/FNa','moderator', 'Vejder');
INSERT INTO user_roles(user_id, role_id) VALUES (currval('users_id_seq'), 3);

-- Create 3 tags
INSERT INTO tags(id, name) VALUES (nextval('tags_id_seq'), 'Java');
INSERT INTO tags(id, name) VALUES (nextval('tags_id_seq'), 'Spring');
INSERT INTO tags(id, name) VALUES (nextval('tags_id_seq'), 'React');
INSERT INTO tags(id, name) VALUES (nextval('tags_id_seq'), 'Postgres');
INSERT INTO tags(id, name) VALUES (nextval('tags_id_seq'), 'English');
INSERT INTO tags(id, name) VALUES (nextval('tags_id_seq'), 'Japanese');
INSERT INTO tags(id, name) VALUES (nextval('tags_id_seq'), 'Russian');
INSERT INTO tags(id, name) VALUES (nextval('tags_id_seq'), 'Redux');
INSERT INTO tags(id, name) VALUES (nextval('tags_id_seq'), 'C++');
INSERT INTO tags(id, name) VALUES (nextval('tags_id_seq'), 'C#');

-- Create ContractAppeals to users with id 1 and 2
insert into contract_appeals(id, user_id, title, describe, charge, created_at, approx_last_day, status) values (nextval('contract_appeals_id_seq'), 1, 'Want to know learn Java and React', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste accusantium architecto magni, dignissimos vitae, natus excepturi incidunt, quis officiis quisquam nulla sed dolore voluptatibus. Nam possimus obcaecati assumenda explicabo vero. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptates facere assumenda quos illum similique, provident eum repellat mollitia qui magni eligendi molestiae perferendis! Harum enim, aspernatur odit dolores eius nostrum?',2000,now(), now(), 'UNCHECKED');
insert into contract_appeals(id, user_id, title, describe, charge, created_at, approx_last_day, status) values (nextval('contract_appeals_id_seq'), 2, 'Have interest in Java', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste accusantium architecto magni, dignissimos vitae, natus excepturi incidunt, quis officiis quisquam nulla sed dolore voluptatibus. Nam possimus obcaecati assumenda explicabo vero. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptates facere assumenda quos illum similique, provident eum repellat mollitia qui magni eligendi molestiae perferendis! Harum enim, aspernatur odit dolores eius nostrum?',1000,now(), now(), 'UNCHECKED');

-- Add 1,2 tags to 1 contract appeal
insert into join_contract_appeal_tag(contract_appeal_id, tag_id) values (1, 1);
insert into join_contract_appeal_tag(contract_appeal_id, tag_id) values (1, 2);

-- Add 1 tag to 2 contract appeal
insert into join_contract_appeal_tag(contract_appeal_id, tag_id) values (2, 1);
