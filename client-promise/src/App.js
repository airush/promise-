import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Navbar from "./components/layout/Navbar";
import UserProfile from "./components/user/UserProfile";
import Dashboard from "./components/dashboard/Dashboard";
import ModeratorDashboard from "./components/moderator/ModeratorDashboard";
import Singin from "./components/auth/Signin";
import SignUp from "./components/auth/Signup";
import CreateContractRequest from "./components/contractRequest/CreateContractRequest";
import ContractRequest from "./components/contractRequest/ContractRequest";
import ReviewContractAppeal from "./components/moderator/ReviewContractAppeal";
import ChatMain from "./components/chat/ChatMain";
import CreateContract from "./components/contract/CreactContract";
import ContactChat from "./components/chat/userChat/ChatMain";
import ContractPage from "./components/contract/Contract";
import Footer from "./components/layout/Footer";
import EditProfile from "./components/user/EditProfile";
import NotFound from "./components/NotFound";
import UserContractAppealPage from "./components/contractRequest/UserContractAppealPage";
import ReviewContracts from "./components/moderator/ReviewContracts";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <main className="App">
          <Navbar />
          <Switch>
            <Route exact path="/" component={Dashboard} />
            <Route path="/moderator/dashboard" component={ModeratorDashboard} />
            <Route exact path="/user/:id(\d+)" component={UserProfile} />
            <Route path="/auth/login" component={Singin} />
            <Route path="/auth/signup" component={SignUp} />
            <Route path="/contract/create" component={CreateContractRequest} />
            <Route
              exact
              path="/contract/request/:id"
              component={ContractRequest}
            />
            <Route
              path="/moderator/contract-appeals"
              component={ReviewContractAppeal}
            />
            <Route path="/moderator/chat" component={ChatMain} />
            <Route
              path="/moderator/contract/create"
              component={CreateContract}
            />
            <Route path="/contacts" component={ContactChat} />
            <Route exact path="/contracts/:id" component={ContractPage} />
            <Route exact path="/user/edit" component={EditProfile} />
            <Route
              exact
              path="/contract-appeals/my"
              component={UserContractAppealPage}
            />
            <Route
              exact
              path="/moderator/contracts"
              component={ReviewContracts}
            />
            <Route component={NotFound} />
          </Switch>
        </main>

        <Footer></Footer>
      </BrowserRouter>
    );
  }
}

export default App;
