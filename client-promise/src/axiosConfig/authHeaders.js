import axios from "axios";

// if doesnt work return import { async } from "q";
const authHeaders = async () => {
  if (localStorage.getItem("access-token")) {
    if (localStorage.getItem("expires-in") - new Date().getTime() < 0) {
      const payload = {
        refreshToken: localStorage.getItem("refresh-token")
      };
      await axios
        .post("http://localhost:8080/api/auth/refresh", payload)
        .then(resp => {
          if (resp.message) {
            console.log("ERROR_MESAGE", resp.message);
          } else {
            const data = resp.data;
            localStorage.setItem("access-token", data.accessToken);
            localStorage.setItem("refresh-token", data.refreshToken);
            localStorage.setItem("expires-in", data.expiresIn);
          }
        })
        .catch(err => {
          if (err.response && err.response.status === 401) {
            window.location.href = "http://localhost:3000/auth/login";
            localStorage.removeItem("access-token");
            localStorage.removeItem("refresh-token");
            localStorage.removeItem("expries-token");
          }
          console.log("Refresh error ", err);
        });
    }
    return {
      Authorization: "Bearer " + localStorage.getItem("access-token")
    };
  }
  localStorage.removeItem("persist:root");
  window.location.href = "http://localhost:3000/auth/login";
};

export default authHeaders;
