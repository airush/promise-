import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
class NotFound extends Component {
  render() {
    return (
      <div
        style={{
          backgroundColor: "#ffffff",
          marginTop: "2px",
          height: "85vh",
          color: "#afbbcb"
        }}
        className="center"
      >
        <Helmet>
          <title>Error 404</title>
        </Helmet>
        <img
          style={{ marginTop: "40px", width: "650px" }}
          src={process.env.PUBLIC_URL + "/images/404.jpg"}
          alt=""
        />
        <h5>
          Go back to{" "}
          <Link
            to="/"
            style={{
              border: "2px solid #3f51b5 ",
              borderRadius: "30px",
              padding: "0 15px 0 15px",
              color: "#788ca8"
            }}
          >
            Dashboard
          </Link>
        </h5>
      </div>
    );
  }
}
export default NotFound;
