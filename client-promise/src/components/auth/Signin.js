import React, { Component } from "react";
import { connect } from "react-redux";
import { userLoginAction } from "../../store/actions/authAction";
import "./SignIn.css";
import { Helmet } from "react-helmet";

class Singin extends Component {
  state = {
    usernameOrEmail: "",
    password: ""
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.userSignIn(this.state);
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.userId) {
      this.props.history.push("/", {
        type: "SUCCESS",
        message: "Logged in successfully"
      });
    }
  }
  render() {
    return (
      <div className="center  container">
        <Helmet>
          <title>Sign in</title>
        </Helmet>
        <div className="sign-in">
          {this.props.location.state && this.props.authError === null ? (
            <div className="green-text text-darken-3">
              {this.props.location.state.signUpSuccessMsg}
            </div>
          ) : null}
          <form onSubmit={this.handleSubmit}>
            <h4>Sign in</h4>
            <div className="input-field">
              <label htmlFor="usernameOrEmail">Email or Login</label>
              <input
                type="text"
                id="usernameOrEmail"
                onChange={this.handleChange}
              />
            </div>
            <div className="input-field">
              <label htmlFor="password">password</label>
              <input
                type="password"
                id="password"
                onChange={this.handleChange}
              />
            </div>

            <div className="input-field">
              <label>
                <input type="checkbox" />
                <span>Remember me</span>
              </label>
              <button type="submit" className="btn z-depth-1">
                Login
              </button>
            </div>
          </form>
          {this.props.authError && (
            <div className="red-text text-darken-3">{this.props.authError}</div>
          )}
          {isFetching(this.props.isFetching)}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    userSignIn: userSignInDetails => {
      dispatch(userLoginAction(userSignInDetails));
    }
  };
};

const isFetching = isFetching => {
  if (isFetching) {
    return (
      <div className="center-align">
        <div className=" preloader-wrapper small active">
          <div className="spinner-layer spinner-blue-only">
            <div className="circle-clipper left">
              <div className="circle" />
            </div>
            <div className="gap-patch">
              <div className="circle" />
            </div>
            <div className="circle-clipper right">
              <div className="circle" />
            </div>
          </div>
        </div>
      </div>
    );
  }
};

const mapStateToProps = state => {
  return {
    userId: state.auth.userId,
    authError: state.auth.authError,
    isFetching: state.auth.isFetching
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Singin);
