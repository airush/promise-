import React, { Component } from "react";
import { userSignUpAction } from "../../store/actions/authAction";
import { connect } from "react-redux";
import "./SignUp.css";
import { Helmet } from "react-helmet";

class SingUp extends Component {
  state = {
    email: "",
    password: "",
    passwordRepeat: "",
    username: "",
    firstName: "",
    lastName: ""
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.userSignUp(this.state);
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.signUpSuccessMsg) {
      this.props.history.push("/auth/login", {
        signUpSuccessMsg: nextProps.signUpSuccessMsg
      });
    }
  }

  render() {
    return (
      <div className="center container">
        <Helmet>
          <title>Sign up</title>
        </Helmet>
        <div className="sign-up">
          <form onSubmit={this.handleSubmit}>
            <h4>Sign up</h4>
            <div className="row">
              <div className="col s6">
                <div className="input-field first-name">
                  <label htmlFor="firstName">First name</label>
                  <input
                    type="text"
                    id="firstName"
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className="col s6">
                <div className="input-field last-name">
                  <label htmlFor="lastName">Last name</label>
                  <input
                    type="text"
                    id="lastName"
                    onChange={this.handleChange}
                  />
                </div>
              </div>
            </div>
            <div className="input-field">
              <label htmlFor="email">Email</label>
              <input type="text" id="email" onChange={this.handleChange} />
            </div>
            <div className="input-field">
              <label htmlFor="username">Username</label>
              <input type="text" id="username" onChange={this.handleChange} />
            </div>
            <div className="input-field">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                onChange={this.handleChange}
              />
            </div>
            <div className="input-field">
              <label htmlFor="passwordRepeat">Repeat password</label>
              <input
                type="password"
                id="passwordRepeat"
                onChange={this.handleChange}
              />
            </div>
            <div className="input-field">
              <button className="btn z-depth-1">Sign up</button>
            </div>
          </form>
          {this.props.authError ? (
            <div className="red-text text-darken-3">{this.props.authError}</div>
          ) : null}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    userSignUp: userInfo => {
      dispatch(userSignUpAction(userInfo));
    }
  };
};

const mapStateToProps = state => {
  return {
    authError: state.auth.authError,
    signUpSuccessMsg: state.auth.signUpSuccessMsg
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SingUp);
