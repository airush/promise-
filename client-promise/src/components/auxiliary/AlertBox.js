import React, { useEffect } from "react";
import "./AlertBox.css";

const AlertBox = ({ color, message }) => {
  function handleClick() {
    window.$("#alert_box").fadeOut("slow", function() {});
  }

  useEffect(() => {
    setTimeout(() => {
      window.$("#alert_box").fadeOut("slow");
    }, 3000);
    return () => {};
  });

  return (
    <div className="container" id="alert-box-id">
      <div className=" row alert-box" id="alert_box">
        <div className="col s12 m12">
          <div className={"card " + color}>
            <div className="row">
              <div className="col s12 m10">
                <div className="card-content white-text">
                  <p>{message}</p>
                </div>
              </div>
              <div className="col s12 m2">
                <i
                  className="material-icons icon_style white-text"
                  id="alert_close"
                  aria-hidden="true"
                  onClick={handleClick}
                >
                  close
                </i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AlertBox;
