import React, { Component } from "react";
import ChatMessage from "./ChatMessage";
import "./ChatBlock.css";
import { connect } from "react-redux";
import { sendMessage } from "../../store/actions/chatAction";

class ChatBlock extends Component {
  state = {
    text: ""
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.sendMessage(this.state);
    this.setState({ text: "" });
  };

  render() {
    console.log("props", this.props);
    const messages = this.props.messages.map(message => (
      <ChatMessage key={message.id} message={message} />
    ));

    return (
      <div style={{ background: "white", border: "1px solid #ede7f6" }}>
        <div style={{ height: "65vh", overflowY: "auto" }}>
          <ul>{messages}</ul>
        </div>
        <div className="chat-block-type-msg">
          <form onSubmit={this.handleSubmit}>
            <div className="input-field">
              <textarea
                id="text"
                className="materialize-textarea"
                data-length="2000"
                placeholder="Type a message"
                value={this.state.text}
                onChange={this.handleChange}
              />
            </div>
            <button className="btn chat-block-type-msg-btn" type="submit">
              Submit
            </button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.webSocketChat.messages
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendMessage: text => {
      dispatch(sendMessage(text));
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatBlock);
