import React, { Component } from "react";
import ContactList from "./ContactList";
import ChatBlock from "./ChatBlock";
import { connect } from "react-redux";
import { chatSetupConnection } from "../../store/actions/chatAction";

class ChatMain extends Component {
  componentDidMount() {
    this.props.chatSetupConnection(this.props.username);
  }

  checkState = () => {
    console.log("ACTUAL props", this.props);
  };
  render() {
    return (
      <div className="container" style={{ marginTop: "15px" }}>
        <div>
          {this.props.webSocketConnectionMsg ? (
            <h6>{this.props.webSocketConnectionMsg}</h6>
          ) : (
            <h6>Connection...</h6>
          )}
        </div>
        <div className="row">
          <div className="col s3">
            <ContactList />
          </div>
          <div className="col s8">
            <ChatBlock />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    username: state.auth.username,
    webSocketConnectionMsg: state.webSocketChat.connectionMessage
  };
};

const mapDispatchToProps = dispatch => {
  return {
    chatSetupConnection: username => {
      dispatch(chatSetupConnection(username));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatMain);
