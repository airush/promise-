import React from "react";
import "./ChatMessage.css";
import moment from "moment";

const ChatMessage = ({ message }) => {
  return (
    <div className="chat-incomming-msg">
      <img
        src={process.env.PUBLIC_URL + "/images/img_avatar.png"}
        alt=""
        className="circle chat-incomming-msg-img"
      />
      <div className="chat-incomming-msg-content-div">
        <h6 className="chat-incomming-msg-username">
          {message.authorFullname}
        </h6>
        <p className="chat-incomming-msg-text">{message.text}</p>
        <span className="chat-incomming-msg-date">
          {moment(message.date).format("lll")}
        </span>
      </div>
    </div>
  );
};

export default ChatMessage;
