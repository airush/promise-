import React from "react";
import "./Contact.css";
const Contract = () => {
  return (
    <div className="contact-elem">
      {/* TODO add status indicator */}
      <img
        src={process.env.PUBLIC_URL + "/images/img_avatar.png"}
        alt=""
        className="circle"
      />
      <h6>Ainur Aimurzin</h6>
    </div>
  );
};

export default Contract;
