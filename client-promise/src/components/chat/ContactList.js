import React, { Component } from "react";
import Contact from "./Contact";

class ContactList extends Component {
  render() {
    const styles = {
      background: "white",
      border: "1px solid #ede7f6",
      height: "65vh",
      overflow: "auto"
    };

    return (
      <div style={styles}>
        <Contact />
        <Contact />
      </div>
    );
  }
}

export default ContactList;
