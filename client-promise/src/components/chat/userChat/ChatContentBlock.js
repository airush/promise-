import React, { Component } from "react";
import ChatMessage from "./ChatMessage";
import "../ChatBlock.css";
import { connect } from "react-redux";
import { sendMessage } from "../../../store/actions/contactChatAction";
import { Link } from "react-router-dom";

class ChatContentBlock extends Component {
  state = {
    text: ""
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleKeyPress = e => {
    if (e.keyCode === 13) {
      console.log("value", e.target.value);
      this.handleSubmit(e);
    }
  };

  handleSubmit = e => {
    e.preventDefault();

    this.props.sendMessage({
      to: this.props.partnerUsername,
      text: this.state
    });
    this.setState({ text: "" });
  };

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "auto" });
  };

  componentDidUpdate() {
    this.scrollToBottom();
  }

  render() {
    const contactId = this.props.curContact
      ? this.props.curContact.contractId
      : null;
    const messages =
      this.props.messages &&
      this.props.messages
        .sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime())
        .map(message => <ChatMessage key={message.id} message={message} />);
    return (
      <div
        style={{
          background: "white",
          border: "1px solid #ede7f6"
        }}
      >
        <div className="chat-content-block-top-links">
          {contactId && (
            <div>
              <div>
                <Link
                  to={"/contracts/" + contactId}
                  className="btn-flat"
                  style={{ color: "#ababab" }}
                >
                  Contract
                  <i className="material-icons left ">note</i>
                </Link>
              </div>
              <div className="right">
                <button className="btn-flat ">
                  Report<i className="material-icons left">error_outline</i>
                </button>
              </div>
            </div>
          )}
        </div>
        <div style={{ height: "60vh", overflowY: "auto", width: "100%" }}>
          <ul>{messages}</ul>
          <div
            style={{ float: "left", clear: "both" }}
            ref={el => {
              this.messagesEnd = el;
            }}
          ></div>
        </div>
        {contactId && (
          <div className="chat-block-type-msg">
            <form onSubmit={this.handleSubmit}>
              <div className="input-field">
                <textarea
                  id="text"
                  className="materialize-textarea"
                  data-length="2000"
                  placeholder="Type a message"
                  value={this.state.text}
                  onChange={this.handleChange}
                  onKeyDown={this.handleKeyPress}
                />
              </div>
              {/* <input type="text" placeholder="Type a message/" /> */}
              <button className="btn chat-block-type-msg-btn" type="submit">
                Submit
              </button>
            </form>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.contactReducer.messages,
    partnerUsername: state.contactReducer.partnerUsername,
    curContact: state.contactReducer.currentContact
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendMessage: message => {
      dispatch(sendMessage(message));
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatContentBlock);
