import React, { Component } from "react";
import ContactList from "./ContactList";
import ChatContentBlock from "./ChatContentBlock";
import { connect } from "react-redux";
import { contactChatSetupConnection } from "../../../store/actions/contactChatAction";
import axios from "axios";
import authHeaders from "../../../axiosConfig/authHeaders";
import { Helmet } from "react-helmet";

class ChatMain extends Component {
  componentDidMount() {
    this.props.contactChatSetupConnection(this.props.username);

    authHeaders().then(headers => {
      axios
        .get("http://localhost:8080/api/contacts", { headers })
        .then(resp => {
          this.props.setContacts(resp.data);
          console.log(resp.data);
          // this.setState({ contacts: resp.data });
        })
        .catch(err => {
          console.log("Error: ", err);
        });
    });
  }

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>Contract Chat</title>
        </Helmet>
        <div className="container" style={{ marginTop: "15px" }}>
          <div>
            {this.props.webSocketConnectionMsg ? (
              <h6>{this.props.webSocketConnectionMsg}</h6>
            ) : (
              <h6>Connection...</h6>
            )}
          </div>
          <div className="row">
            {/* <div className="col offset-s1" /> */}
            <div className="col s3">
              <ContactList contacts={this.props.contacts} />
            </div>
            <div className="col s8">
              <ChatContentBlock />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    username: state.auth.username,
    webSocketConnectionMsg: state.webSocketChat.connectionMessage,
    contacts: state.contactReducer.contacts
  };
};

const mapDispatchToProps = dispatch => {
  return {
    contactChatSetupConnection: username => {
      dispatch(contactChatSetupConnection(username));
    },
    setContacts: contacts => {
      dispatch({ type: "SET_CONTACTS", contacts: contacts });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatMain);
