import React from "react";
import "../ChatMessage.css";
import moment from "moment";
import { connect } from "react-redux";
const ChatMessage = props => {
  return (
    <div className="chat-incomming-msg">
      <img
        src={
          props.message.avatarUrl
            ? props.message.avatarUrl
            : process.env.PUBLIC_URL + "/images/img_avatar.png"
        }
        alt=""
        className="circle chat-incomming-msg-img"
      />
      <div className="chat-incomming-msg-content-div">
        <h6
          style={{ marginLeft: "5px" }}
          className="chat-incomming-msg-username"
        >
          {/* {props.message.authorFullname} */}
          {props.message.userId === props.myId
            ? props.myFirstName + " " + props.myLastName
            : props.partnerName}
        </h6>
        <p className="chat-incomming-msg-text">{props.message.text}</p>
        <span className="chat-incomming-msg-date">
          {moment(props.message.date).format("lll")}
        </span>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    partnerName: state.contactReducer.fullname,
    myFirstName: state.auth.firstName,
    myLastName: state.auth.lastName,
    myId: state.auth.userId
  };
};

export default connect(mapStateToProps)(ChatMessage);
