import React from "react";
import "../Contact.css";
import { connect } from "react-redux";

const ContactItem = props => {
  var styles =
    props.contact.username === props.partnerUsername
      ? { backgroundColor: "#e3e3e3", paddingTop: "5px", paddingBottom: "5px" }
      : { paddingTop: "5px", paddingBottom: "5px" };

  const getMessages = () => {
    const fullname = props.contact.firstName + " " + props.contact.secondName;
    props.getMessages(props.contact.id, fullname);
  };

  return (
    <div style={styles} className="contact-elem" onClick={getMessages}>
      {/* TODO add status indicator */}
      <img
        src={
          props.contact.avatarUrl
            ? props.contact.avatarUrl
            : process.env.PUBLIC_URL + "/images/img_avatar.png"
        }
        alt=""
        className="circle"
      />
      <h6 style={{ marginTop: "5px" }}>
        {props.contact.firstName} {props.contact.secondName}
      </h6>
    </div>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    getMessages: (id, fullname) => {
      dispatch({
        type: "SET_CURRENT_CONTACT_MESSAGES",
        id: id,
        fullname
      });
    }
  };
};

const mapStateToProps = state => {
  return {
    partnerUsername: state.contactReducer.partnerUsername
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactItem);
