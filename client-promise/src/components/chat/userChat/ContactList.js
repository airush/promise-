import React from "react";
import ContactItem from "./ContactItem";

const ContactList = ({ contacts }) => {
  console.log("Contacts", contacts);
  const contactListItems = contacts ? (
    contacts.map(item => <ContactItem key={item.id} contact={item} />)
  ) : (
    <h6>loading...</h6>
  );

  const styles = {
    background: "white",
    border: "1px solid #ede7f6",
    height: "65vh",
    overflow: "auto"
  };
  return <div style={styles}>{contactListItems}</div>;
};
export default ContactList;
