import React, { Component } from "react";
import moment from "moment";
import axios from "axios";
import { Link } from "react-router-dom";
import authHeaders from "../../axiosConfig/authHeaders";
import { connect } from "react-redux";

class Contract extends Component {
  state = {
    isSubmitDisabled: false
  };

  componentDidMount() {
    const id = this.props.match.params.id;
    authHeaders().then(headers => {
      axios
        .get("http://localhost:8080/api/contract/" + id, { headers })
        .then(resp => {
          this.setState({ contract: resp.data });
          console.log("Resp datas", resp.data);
          var collapsible = document.querySelectorAll(".collapsible");

          window.M.Collapsible.init(collapsible, {});
        })
        .catch(err => {
          console.log(err.response);
          console.log("Error: ", err);
        });
    });
  }

  confirmConnection = () => {
    this.setState({
      ...this.state,
      isSubmitDisabled: true
    });
    // authHeaders().then(jwt => {
    const payload = { id: this.props.match.params.id };
    axios
      .post("http://localhost:8080/api/contract/connect", payload, {
        headers: {
          // Authorization: jwt.Authorization,
          "Content-Type": "application/json"
        }
      })
      .then(resp => {
        console.log(resp.data);
      })
      .catch(err => {
        console.log(err.response);
        console.log("Error: ", err);
      });
    // });
  };

  render() {
    const contract = this.state.contract;
    return (
      <div className="container">
        {contract && (
          <div>
            <div style={{ position: "relative" }} className="contract-appeal">
              <div className="right">
                <h6>
                  Created at:{" "}
                  {moment(contract.createdAt)
                    .add(-5, "hours")
                    .format("LL")}
                </h6>
              </div>

              <h3>{contract.title}</h3>

              <h6>
                Moderator:{" "}
                <Link to={"/user/" + contract.moderatorId}>
                  {contract.moderatorUsername}
                </Link>
              </h6>

              <div className="divider" />
              <div className="row">
                <div className="col s4">
                  <img
                    className="left"
                    src={process.env.PUBLIC_URL + "/images/contract.jpg"}
                    alt="Searching your partner"
                  />
                </div>
                <div className="col s8">
                  <h6>Contract details</h6>
                  <blockquote style={{ marginRight: "25px" }}>
                    {contract.details}
                  </blockquote>
                  {/* Tags */}
                  {/* <div className="tags"> */}
                  {/* {contractAppeal.tagSet.map(tag => (
                    // <div key={tag.id} className="chip">
                    //    
                    //   {tag.name}
                    // </div> */}
                  {/* ))}
              </div> */}
                </div>
              </div>
              <div align="center" style={{ marginBottom: 40 }}>
                <ul className="collapsible popout">
                  <li>
                    <div className="collapsible-header">
                      <Link
                        to={"/user/" + contract.firstContractAppeal.authorId}
                        className="btn btn-small btn-floating pink accent-1 z-depth-0"
                      >
                        {/* authorAvatarUrl */}
                        {contract.firstContractAppeal.authorAvatarUrl ? (
                          <img
                            style={{ height: "40px", width: "40px" }}
                            src={contract.firstContractAppeal.authorAvatarUrl}
                            alt=""
                          />
                        ) : (
                          contract.firstContractAppeal.authorUsername[0].toUpperCase()
                        )}
                      </Link>
                      <h6 style={{ margin: "8px 0 0 15px" }}>
                        {contract.firstContractAppeal.title}
                      </h6>
                    </div>

                    <div align="left" className="collapsible-body">
                      <p>User: {contract.firstContractAppeal.authorUsername}</p>
                      <p>
                        Request created at:{" "}
                        {moment(contract.firstContractAppeal.createdAt).format(
                          "LLL"
                        )}
                      </p>
                      <p>
                        ApproxLastDay:{" "}
                        {moment(
                          contract.firstContractAppeal.approxLastDay
                        ).format("LL")}
                      </p>
                      <p>Charge: {contract.firstContractAppeal.charge}</p>
                      <p>Description: {contract.firstContractAppeal.title}</p>
                    </div>
                  </li>
                  <li>
                    <div className="collapsible-header">
                      <Link
                        to={"/user/" + contract.secondContractAppeal.authorId}
                        className="btn btn-small btn-floating pink accent-1 z-depth-0"
                      >
                        {contract.secondContractAppeal.authorAvatarUrl ? (
                          <img
                            style={{ height: "40px", width: "40px" }}
                            src={contract.secondContractAppeal.authorAvatarUrl}
                            alt=""
                          />
                        ) : (
                          contract.secondContractAppeal.authorUsername[0].toUpperCase()
                        )}
                      </Link>
                      <h6 style={{ margin: "8px 0 0 15px" }}>
                        {contract.secondContractAppeal.title}
                      </h6>
                    </div>

                    <div align="left" className="collapsible-body">
                      <p>
                        User: {contract.secondContractAppeal.authorUsername}
                      </p>
                      <p>
                        Request created at:{" "}
                        {moment(contract.secondContractAppeal.createdAt).format(
                          "LLL"
                        )}
                      </p>
                      <p>
                        ApproxLastDay:{" "}
                        {moment(
                          contract.secondContractAppeal.approxLastDay
                        ).format("LL")}
                      </p>
                      <p>Charge: {contract.secondContractAppeal.charge}</p>
                      <p>Description: {contract.secondContractAppeal.title}</p>
                    </div>
                  </li>
                </ul>
              </div>
              {this.props.roles.filter(o => o.name === "ROLE_MODERATOR")
                .length > 0 && (
                <button
                  onClick={this.confirmConnection}
                  style={{
                    bottom: 20,
                    right: 0,
                    position: "absolute",
                    marginRight: "10px"
                  }}
                  className="btn right green"
                  disabled={this.state.isSubmitDisabled}
                >
                  Confirm contract
                </button>
              )}
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    roles: state.auth.roles
  };
};

export default connect(mapStateToProps)(Contract);
