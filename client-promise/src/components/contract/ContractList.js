import React from "react";
import ContractListItem from "./ContractListItem";
import "../contractRequest/ContractRequestList.css";

const ContractList = ({ data }) => {
  console.log("Coming data", data);
  const contracts = data.contracts ? (
    data.contracts.map(item => <ContractListItem key={item.id} item={item} />)
  ) : (
    <h6>No data</h6>
  );
  return <ul className="collection contract-appeal-list">{contracts}</ul>;
};

export default ContractList;
