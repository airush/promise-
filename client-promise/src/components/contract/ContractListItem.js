import React from "react";
import "../contractRequest/ContractRequestListItem.css";
import { Link } from "react-router-dom";
import moment from "moment";

const ContractListItem = ({ item }) => {
  return (
    <li className="contract-appeal-item">
      <div className="contract-appeal-item-first-block">
        <img
          src={process.env.PUBLIC_URL + "/images/connection.png"}
          alt=""
          className=""
        />
      </div>
      <div className="contract-appeal-item-second-block">
        <Link to={"/contracts/" + item.id}>
          <h6>{item.title}</h6>
        </Link>
        <p className="contract-appeal-list-item-description ">{item.details}</p>
      </div>
      <div className="contract-appeal-item-third-block right">
        <span>
          {moment(item.createdAt)
            .add(-5, "hours")
            .format("LL")}
        </span>
      </div>
    </li>
  );
};

export default ContractListItem;
