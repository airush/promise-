import React, { Component } from "react";
import { connect } from "react-redux";
import "./CreateContract.css";
import { createContract } from "../../store/actions/moderContractActions";
import moment from "moment";

class CreateContract extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstContractAppeal: this.props.firstContractAppeal,
      secondContractAppeal: this.props.secondContractAppeal,
      moderatorId: this.props.moderatorId
    };
  }

  componentDidMount() {
    // Enable materialize js
    var collapsible = document.querySelectorAll(".collapsible");
    window.M.Collapsible.init(collapsible, {});

    var datepicker = document.querySelectorAll(".datepicker");
    window.M.Datepicker.init(datepicker, {});

    window.$("input#title, textarea#describe").characterCounter();

    window.$(".datepicker-done").click(() => {
      var datepickerValue = window.$("#approxLastDay").val();

      this.setState({
        approxLastDay: moment(datepickerValue, "MMM D, YYYY").toDate()
      });
    });
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();

    this.props.createContract(this.state);

    this.props.resetBothContractAppealRequests();

    this.props.history.push("/moderator/contract-appeals");
  };

  render() {
    return (
      <div className="container-fluid">
        <div align="center" className="container">
          <div className="create-contract">
            <form onSubmit={this.handleSubmit}>
              <h5 align="left" className="">
                Draw up contract
              </h5>
              <div className="divider" />
              <div className="input-field">
                <i className="material-icons prefix">title</i>
                <input
                  type="text"
                  id="title"
                  data-length="50"
                  onChange={this.handleChange}
                  onSelect={this.handleChange}
                />
                <label htmlFor="title">Title</label>
              </div>

              <div className="input-field">
                <i className="material-icons prefix">create</i>
                <textarea
                  id="describe"
                  className="materialize-textarea"
                  data-length="2000"
                  onChange={this.handleChange}
                />
                <label htmlFor="describe">Contract details</label>
              </div>

              <div className="row">
                <div className="col s6">
                  <div className="input-field">
                    <i className="material-icons prefix">attach_money</i>
                    <label htmlFor="charge">Final charge</label>
                    <input
                      type="number"
                      id="charge"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <div className="col s6">
                  <div id="approx-date-container">
                    <i className="material-icons prefix">date_range</i>
                    <input
                      id="approxLastDay"
                      className="datepicker"
                      placeholder="Chouse date"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
              </div>

              <div align="center">
                <ul className="collapsible popout">
                  <li>
                    <div className="collapsible-header">
                      <button className="btn btn-small btn-floating pink accent-1 z-depth-0">
                        {this.state.firstContractAppeal.authorUsername[0].toUpperCase()}
                      </button>
                      <h6 style={{ margin: "8px 0 0 15px" }}>
                        {this.state.firstContractAppeal.title}
                      </h6>
                    </div>

                    <div align="left" className="collapsible-body">
                      <p>
                        User: {this.state.firstContractAppeal.authorUsername}
                      </p>
                      <p>
                        Request created at:{" "}
                        {moment(
                          this.state.firstContractAppeal.createdAt
                        ).format("LLL")}
                      </p>
                      <p>
                        ApproxLastDay:{" "}
                        {moment(
                          this.state.firstContractAppeal.approxLastDay
                        ).format("LL")}
                      </p>
                      <p>Charge: {this.state.firstContractAppeal.charge}</p>
                      <p>Description: {this.state.firstContractAppeal.title}</p>
                    </div>
                  </li>
                  <li>
                    <div className="collapsible-header">
                      <button className="btn btn-small btn-floating pink accent-1 z-depth-0">
                        {this.state.secondContractAppeal.authorUsername[0].toUpperCase()}
                      </button>
                      <h6 style={{ margin: "8px 0 0 15px" }}>
                        {this.state.secondContractAppeal.title}
                      </h6>
                    </div>

                    <div align="left" className="collapsible-body">
                      <p>
                        User: {this.state.secondContractAppeal.authorUsername}
                      </p>
                      <p>
                        Request created at:{" "}
                        {moment(
                          this.state.secondContractAppeal.createdAt
                        ).format("LLL")}
                      </p>
                      <p>
                        ApproxLastDay:{" "}
                        {moment(
                          this.state.secondContractAppeal.approxLastDay
                        ).format("LL")}
                      </p>
                      <p>Charge: {this.state.secondContractAppeal.charge}</p>
                      <p>
                        Description: {this.state.secondContractAppeal.title}
                      </p>
                    </div>
                  </li>
                </ul>
              </div>

              <div style={{ marginTop: "25px" }} className="right input-field">
                <button
                  className="btn red z-depth-0"
                  type="button"
                  onClick={e => {
                    this.props.resetBothContractAppealRequests();
                    this.props.history.push("/moderator/contract-appeals");
                  }}
                >
                  Cancel both<i className="material-icons right">cancel</i>
                </button>
                <button
                  style={{ marginLeft: "15px" }}
                  className="btn z-depth-0"
                  onClick={this.handleSubmit}
                >
                  Submit<i className="material-icons right">send</i>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    firstContractAppeal: state.moderContractAppeal.contractAppealTarget,
    secondContractAppeal: state.moderContractAppeal.contractAppealTargetPair,
    moderatorId: state.auth.userId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createContract: contract => {
      dispatch(createContract(contract));
    },
    resetBothContractAppealRequests: () =>
      dispatch({ type: "RESET_BOTH_CONTRACT_APPEAL_REQUESTS" })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateContract);
