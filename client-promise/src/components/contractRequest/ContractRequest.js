import React, { Component } from "react";
import "./ContractRequest.css";
import axios from "axios";
import moment from "moment";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  connectToContractRequest,
  connectToContractRequestSecond
} from "../../store/actions/moderContractRequestAction";
import authHeaders from "../../axiosConfig/authHeaders";
import { Helmet } from "react-helmet";
class ContractRequest extends Component {
  state = {};

  componentDidMount() {
    const id = this.props.match.params.id;

    axios
      .get("http://localhost:8080/api/contract_appeal/" + id, {})
      .then(resp => {
        this.setState({ contractAppeal: resp.data });
      })
      .catch(err => {
        console.log("Error: ", err);
      });
  }

  connectToContractRequest = () => {
    this.props.connectToContractRequest(this.state);
    this.props.history.push("/moderator/contract-appeals");
  };

  connectToContractRequestSecond = () => {
    this.props.connectToContractRequestSecond(this.state);
    this.props.history.push("/moderator/contract/create");
  };

  render() {
    const contractAppeal = this.state.contractAppeal;
    //  ==> START Buttons
    var buttons;
    if (
      this.props.userRoles.filter(o => o.name === "ROLE_MODERATOR").length !== 0
    ) {
      // Render confirm buttons to connect to contract appeal
      if (
        this.props.contractAppealTarget &&
        parseInt(this.props.contractAppealTarget.id) !==
          parseInt(this.props.match.params.id)
      ) {
        buttons = (
          <div className="contract-appeal-btns">
            <button
              style={{ marginRight: "25px" }}
              className="btn yellow darken-4 right"
            >
              Back to rewrite
            </button>
            <button
              onClick={this.connectToContractRequestSecond}
              style={{ marginRight: "10px" }}
              className="btn right green"
            >
              Connect
            </button>
          </div>
        );
      }
      // Render when no target
      else {
        buttons = (
          <div className="contract-appeal-btns">
            <button
              style={{ marginRight: "25px" }}
              className="btn yellow darken-4 right"
            >
              Back to rewrite
            </button>
            <button
              onClick={this.connectToContractRequest}
              style={{ marginRight: "10px" }}
              className="btn right"
            >
              Find similar
            </button>
          </div>
        );
      }
    }
    // for user
    else {
      buttons = (
        <div className="contract-appeal-btns">
          <button
            style={{ marginRight: "25px" }}
            className="btn red darken-1 right"
            onClick={e => {
              authHeaders().then(headers => {
                axios
                  .post(
                    "http://localhost:8080/api/contract_appeals/delete/" +
                      this.props.match.params.id,
                    {},
                    { headers }
                  )
                  .then(resp => {
                    console.log("Response data: ", resp.data);
                    this.props.history.push("/contract-appeals/my", {
                      type: "SUCCESS",
                      message: "Contract appeal deleted!"
                    });
                  })
                  .catch(err => {
                    console.log("Error: ", err);
                  });
              });
            }}
          >
            Cancel
          </button>
          <button style={{ marginRight: "10px" }} className="btn right">
            Edit
          </button>
        </div>
      );
    }
    // <== END Buttons
    return (
      <div className="container">
        {contractAppeal ? (
          <div style={{ position: "relative" }} className="contract-appeal">
            <Helmet>
              <title>Contract appeal {contractAppeal.title}</title>
            </Helmet>
            <div className="right">
              <h6>
                Created at:{" "}
                {moment(contractAppeal.createdAt)
                  .add(-5, "hours")
                  .format("LL")}
              </h6>
              <h6>
                Approx last day:{" "}
                {moment(contractAppeal.approxLastDay)
                  .add(-5, "hours")
                  .format("LL")}
              </h6>
            </div>

            <h3>{contractAppeal.title}</h3>

            <h6>
              author:{" "}
              <Link to={"/users/" + contractAppeal.userId}>
                {contractAppeal.authorUsername}
              </Link>
            </h6>

            <div className="divider" />
            <div className="row">
              <div className="col s4">
                <h6>Status: {contractAppeal.status}</h6>
                <img
                  className="left"
                  src={
                    process.env.PUBLIC_URL + "/images/contract-appeal-img.jpg"
                  }
                  alt="Searching your partner"
                />
              </div>
              <div className="col s8">
                <h6>Description</h6>
                <blockquote style={{ marginRight: "25px" }}>
                  {contractAppeal.describe}
                </blockquote>
                {/* Tags */}
                <div className="tags">
                  {contractAppeal.tagSet.map(tag => (
                    <div key={tag.id} className="chip">
                      {tag.name}
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div style={{ bottom: 20, right: 0, position: "absolute" }}>
              {/* Buttons */}
              {buttons}
            </div>
          </div>
        ) : (
          <h5>Loading...</h5>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    userRoles: state.auth.roles,
    contractAppealTarget: state.moderContractAppeal.contractAppealTarget
  };
};

const mapDispatchToProps = dispatch => {
  return {
    connectToContractRequest: contractAppeal =>
      dispatch(connectToContractRequest(contractAppeal)),
    connectToContractRequestSecond: contractAppeal =>
      dispatch(connectToContractRequestSecond(contractAppeal))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContractRequest);
