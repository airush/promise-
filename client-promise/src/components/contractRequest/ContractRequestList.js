import React from "react";
import "./ContractRequestList.css";
import ContractRequestListItem from "./ContractRequestListItem";
// ({contractRequests}) sended by  ModeratorDashboard
const ContractRequestList = ({ data }) => {
  const contractAppealListItems =
    data.contractAppealsSet && data.contractAppealsSet.length > 0 ? (
      data.contractAppealsSet.map(item => (
        <ContractRequestListItem key={item.id} item={item} />
      ))
    ) : (
      <h6>No contract appeals</h6>
    );
  return (
    <ul className="collection contract-appeal-list">
      {contractAppealListItems}
    </ul>
  );
};

export default ContractRequestList;
