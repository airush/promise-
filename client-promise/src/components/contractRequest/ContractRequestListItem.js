import React from "react";
import "./ContractRequestListItem.css";
import { Link } from "react-router-dom";
import moment from "moment";

const ContractRequestListItem = ({ item }) => {
  const tags = item.tagSet ? (
    item.tagSet.map(tag => (
      <div key={tag.id} className="chip">
        {tag.name}
      </div>
    ))
  ) : (
    <h6>No tags</h6>
  );
  return (
    <li className="contract-appeal-item">
      <div className="contract-appeal-item-first-block">
        <img
          src={
            item.avatarUrl
              ? item.avatarUrl
              : process.env.PUBLIC_URL + "/images/contract.png"
          }
          alt=""
          className=""
        />
      </div>
      <div className="contract-appeal-item-second-block">
        <Link to={"/contract/request/" + item.id}>
          <h6>{item.title}</h6>
        </Link>
        <p className="contract-appeal-list-item-description ">
          {item.describe}
        </p>
      </div>
      <div className="contract-appeal-item-third-block right">
        <span>
          {moment(item.createdAt)
            .add(-5, "hours")
            .format("LL")}
        </span>
      </div>
      <div>
        {tags}
        {/* <div className="chip">Java</div> */}
      </div>
    </li>
  );
};

export default ContractRequestListItem;
