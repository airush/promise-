import React, { Component } from "react";
import { connect } from "react-redux";
import "./CreateContractRequest.css";
import axios from "axios";
import moment from "moment";
import { Helmet } from "react-helmet";
import authHeaders from "../../axiosConfig/authHeaders";

class CreateContractRequest extends Component {
  state = {
    title: "",
    describe: "",
    charge: null,
    approxLastDay: undefined,
    tags: [],
    isSubmitDisavled: false
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ isSubmitDisavled: true });

    authHeaders().then(headers => {
      axios
        .post(
          "http://localhost:8080/api/contract_appeal/create",
          this.state,

          { headers }
        )
        .then(resp => {
          console.log("Resp", resp.data);
          this.props.history.push("/contract/request/" + resp.data);
        })
        .catch(err => {
          this.setState({ isSubmitDisavled: false });
          console.log(err);
        });
    });
  };

  componentDidMount() {
    // Enable materialize js
    var chips = document.querySelectorAll(".chips");

    // Get tags from data base
    axios.get("http://localhost:8080/api/tags").then(resp => {
      console.log("RSEP", resp);
      window.M.Chips.init(chips, {
        //  add Tag to the State
        onChipAdd: (e, chip) => {
          const tagWithCloseString = chip.textContent;
          const tags = this.state.tags.concat(
            tagWithCloseString.substring(0, tagWithCloseString.length - 5)
          );
          this.setState({
            ...this.state,
            tags
          });
        },
        autocompleteOptions: { data: resp.data, limit: Infinity, minLength: 1 }
      });
    });
    var datepicker = document.querySelectorAll(".datepicker");
    window.M.Datepicker.init(datepicker, {});

    window.$("input#title, textarea#describe").characterCounter();

    window.$(".datepicker-done").click(() => {
      var datepickerValue = window.$("#approxLastDay").val();
      this.setState({
        approxLastDay: moment(datepickerValue, "MMM D, YYYY")
          .add(1, "days")
          .toDate()
      });
    });
  }

  render() {
    return (
      <div className="container-fluid">
        <Helmet>
          <title>Create contract appeal</title>
        </Helmet>
        <div align="center" className="container">
          <div align="left" className="create-request">
            <form onSubmit={this.handleSubmit}>
              <h5 className="center">Create promise</h5>
              <div className="row">
                <div className="col s6">
                  <div className="input-field">
                    <label htmlFor="title">Title</label>
                    <input
                      type="text"
                      id="title"
                      data-length="50"
                      onChange={this.handleChange}
                      onSelect={this.handleChange}
                    />
                  </div>
                </div>
                <div className="col s6" id="approx-date-container">
                  <input
                    id="approxLastDay"
                    className="datepicker"
                    placeholder="Chouse date"
                    onChange={this.handleChange}
                  />
                </div>
              </div>

              <div className="input-field">
                <label htmlFor="charge">Charge</label>
                <input type="number" id="charge" onChange={this.handleChange} />
              </div>

              <div className="input-field">
                <textarea
                  id="describe"
                  className="materialize-textarea"
                  data-length="2000"
                  onChange={this.handleChange}
                />
                <label htmlFor="describe">
                  Describe what do you want to promise?
                </label>
              </div>
              <div className="chips chips-placeholder input-field">
                <input id="tags" placeholder="Enter a tag" />
              </div>

              <div className="right input-field">
                <button
                  disabled={this.state.isSubmitDisavled}
                  className="btn z-depth-0"
                >
                  Create<i className="material-icons right">send</i>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    userId: state.auth.userId
  };
};

export default connect(mapStateToProps)(CreateContractRequest);
