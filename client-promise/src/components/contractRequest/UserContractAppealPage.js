import React, { Component } from "react";
import axios from "axios";
import ContractRequestList from "./ContractRequestList";
import authHeaders from "../../axiosConfig/authHeaders";
import LoadingCircle from "../auxiliary/LoadingCircle";
import AlertBox from "../auxiliary/AlertBox";

class UserContractAppeal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    authHeaders().then(headers => {
      axios
        .get("http://localhost:8080/api/contract-appeals/my", { headers })
        .then(resp => {
          console.log("Success resp,", resp.data);
          this.setState({ contractAppealsSet: resp.data });
        })
        .catch(err => {
          console.log("Error: ", err);
        });
    });
  }

  render() {
    return (
      <React.Fragment>
        {this.props.location.state &&
          this.props.location.state.type === "SUCCESS" && (
            <AlertBox
              color={" green"}
              message={this.props.location.state.message}
            ></AlertBox>
          )}
        <div className="container" style={{ marginTop: "35px" }}>
          {this.state.contractAppealsSet ? (
            <div className="row">
              <div className="col s7 offset-s2">
                <ContractRequestList data={this.state} />
              </div>
            </div>
          ) : (
            <LoadingCircle />
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default UserContractAppeal;
