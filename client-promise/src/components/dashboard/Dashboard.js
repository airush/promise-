import React, { Component } from "react";
import axios from "axios";
import authHeaders from "../../axiosConfig/authHeaders";
import "./Dashboard.css";
import AlertBox from "../auxiliary/AlertBox";
import { Helmet } from "react-helmet";

const TITLE = "Promise dashboard";

class Dashboard extends Component {
  onClickHandler = () => {
    authHeaders()
      .then(headers => {
        axios
          .get("http://localhost:8080/api/private", {
            headers
          })
          .then(resp => console.log(resp))
          .catch(err => {
            console.log("Dashboard Error message: ", err.message);
          });
      })
      .catch(err => console.log(err.message));
  };

  onSecondClickHandler = () => {
    axios
      .get("http://localhost:8080/api/public")
      .then(resp => console.log(resp))
      .catch(err => {
        console.log("Second button err ", err.message);
      });
  };

  onThirdClickHandler = () => {
    authHeaders()
      .then(headers => {
        axios
          .get("http://localhost:8080/api/moderator", {
            headers
          })
          .then(resp => console.log(resp))
          .catch(err => {
            console.log("Dashboard Error message: ", err.message);
          });
      })
      .catch(err => console.log(err.message));
  };

  onWsClickHandler = () => {
    authHeaders().then(headers => {
      axios
        .get("http://localhost:8080/api/contacts", { headers })
        .then(resp => {
          console.log("Success resp,", resp);
          this.setState({ contractAppealsSet: resp.data });
        })
        .catch(err => {
          console.log("Error: ", err);
        });
    });
  };
  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>{TITLE}</title>
        </Helmet>
        {this.props.location.state &&
          this.props.location.state.type === "SUCCESS" && (
            <AlertBox
              color={" green"}
              message={this.props.location.state.message}
            ></AlertBox>
          )}
        <div style={{ marginTop: "15px" }} className="container center white">
          <div className="main-dashboard">
            <div className="center">
              <h4>Some cool phrase</h4>
              <h6>Second cool phrase - design trick</h6>
              <div className="divider" />
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo at
                aperiam aliquam eos cupiditate voluptas veritatis, nisi pariatur
                eveniet sed nihil vel, excepturi debitis odio error. Mollitia
                facere porro quia. Lorem ipsum dolor sit amet consectetur
                adipisicing elit. Non quibusdam nostrum reiciendis voluptas
                adipisci omnis dignissimos, necessitatibus et voluptate quas.
                Ipsa dignissimos mollitia veritatis vero corporis doloribus
                repudiandae aliquam iste. Lorem
              </p>
            </div>
            <img
              className="dashboard-img"
              src={process.env.PUBLIC_URL + "/images/dashboard1.jpg"}
              alt=""
            />
            <div className="section-2">
              <h4 className="left"> Why you should start using this site?</h4>
              <div className="divider" />
              <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                Nesciunt nam dolorum aspernatur, debitis facilis ullam
                voluptatem quaerat explicabo, quod commodi nisi ea magnam.
                Doloribus ea eligendi itaque nihil exercitationem illum.
              </p>
              <div className="row">
                <div className="col s12 right-align">
                  <img
                    className="left"
                    src={process.env.PUBLIC_URL + "/images/challenge.png"}
                    alt=""
                  />
                  <h5>Challenge yourself</h5>
                  <p>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Ipsa dolores dicta ut explicabo ab, delectus neque
                    perferendis ratione, enim, autem eveniet nostrum maiores
                    quas accusantium. Sint dolorum molestias libero
                    exercitationem!
                  </p>
                </div>
                <div className="col s12 left-align">
                  <img
                    className="right"
                    alt=""
                    src={process.env.PUBLIC_URL + "/images/competition.png"}
                  />
                  <h5>It is fun</h5>
                  <p>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Ipsa dolores dicta ut explicabo ab, delectus neque
                    perferendis ratione, enim, autem eveniet nostrum maiores
                    quas accusantium. Sint dolorum molestias libero
                    exercitationem!
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Dashboard;
