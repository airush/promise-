import React from "react";

const Footer = () => {
  return (
    <footer
      className="page-footer"
      style={{ backgroundColor: "#3f4e5f", padding: "12px 0 7px 0" }}
    >
      <div className="container">
        © 2019 Kazan ITIS
        <a className="grey-text text-lighten-4 right" href="#!">
          Gmail: aliveairush@gmail.com
        </a>
      </div>
    </footer>
  );
};

export default Footer;
