import React from "react";
import { Link } from "react-router-dom";
import SignedOutLinks from "./SignedOutLinks";
import "./Navbar.css";
import SignInLinks from "./SignInLinks";
import { connect } from "react-redux";

import { logoutAction } from "../../store/actions/authAction";
const Navbar = props => {
  const { initials, userId } = props;
  const logout = () => {
    props.logout();
  };

  const links = userId ? (
    <SignInLinks logout={logout} initials={initials} userId={userId} />
  ) : (
    <SignedOutLinks />
  );

  return (
    <nav className="nav-wrapper red lighten-1">
      <div className="container">
        <Link to="/" className="brand-logo">
          <h3>Promise</h3>
          <img
            src={process.env.PUBLIC_URL + "/images/strategy.png"}
            alt=""
            className=""
          />
        </Link>
        {links}
      </div>
    </nav>
  );
};

const mapStateToProps = state => {
  return {
    initials: state.auth.initials,
    userId: state.auth.userId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => {
      dispatch(logoutAction());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar);
