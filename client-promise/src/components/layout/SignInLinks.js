import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

const SignInLinks = props => {
  useEffect(() => {
    var dropdownTrigger = document.querySelectorAll(".dropdown-trigger");
    window.M.Dropdown.init(dropdownTrigger, {
      hover: false,
      coverTrigger: false
    });
  }, []);

  const menuList =
    props.roles.filter(o => o.name === "ROLE_MODERATOR").length > 0 ? (
      <ul id="dropdown1" className="dropdown-content">
        <li>
          <Link to="/moderator/contract-appeals">
            <i className="material-icons">navigate_next</i> Contract appeals
          </Link>
        </li>
        <li>
          <Link to="/moderator/contracts">
            <i className="material-icons">navigate_next</i> Created contracts
          </Link>
        </li>
        <li>
          <Link to="/moderator/chat">
            <i className="material-icons">navigate_next</i> Moder chat
          </Link>
        </li>
        <li className="divider" tabIndex="-1"></li>
        <li>
          <Link to="/contract/create">
            <i className="material-icons">create</i>Create contract appeal
          </Link>
        </li>
        <li>
          <Link to="/contacts">
            <i className="material-icons">group</i>Contacts
          </Link>
        </li>
      </ul>
    ) : (
      <ul id="dropdown1" className="dropdown-content">
        <li>
          <Link to="/contract/create">
            <i className="material-icons">create</i>Create contract appeal
          </Link>
        </li>
        <li>
          <Link to="/contacts">
            <i className="material-icons">group</i>Contacts
          </Link>
        </li>
        <li>
          <Link to="/contract-appeals/my">
            <i className="tiny material-icons">subject</i>My contract appeals
          </Link>
        </li>
      </ul>
    );

  return (
    <ul className="right">
      <li className="navbar-menu-dropdown">
        <a
          className="dropdown-trigger btn-flat white red-text lighten-2-text"
          href="!#"
          data-target="dropdown1"
        >
          Menu
        </a>

        {/* <!-- Dropdown Structure --> */}
        {menuList}
      </li>
      <li className="navbar-menu-dropdown">
        <a
          className="dropdown-trigger btn btn-floating green lighten-2 z-depth-0"
          href={"/user/" + props.userId}
          data-target="dropdown2"
          style={{ border: "1px solid #f7a6a4" }}
        >
          {props.avatarUrl ? (
            <img
              style={{ height: "40px", width: "40px" }}
              src={props.avatarUrl}
              alt=""
            />
          ) : (
            props.initials.toUpperCase()
          )}
        </a>
      </li>
      <ul id="dropdown2" className="dropdown-content">
        <li>
          <Link to={"/user/" + props.userId}>
            <i className="material-icons">person</i>Profile
          </Link>
        </li>
        <li>
          <Link to="/user/edit">
            <i className="material-icons">settings</i>Settings
          </Link>
        </li>
        <li>
          <Link to="/auth/login" onClick={props.logout}>
            <i className="material-icons">exit_to_app</i>Logout
          </Link>
        </li>
      </ul>
    </ul>
  );
};

const mapStateToProps = state => {
  return {
    roles: state.auth.roles,
    avatarUrl: state.auth.avatarUrl
  };
};

export default connect(mapStateToProps)(SignInLinks);
