import React, { Component } from "react";
import UserList from "./UserList";
import ContractRequestList from "../contractRequest/ContractRequestList";
import { connect } from "react-redux";

class ModeratorDashboard extends Component {
  render() {
    // const{} object destructuring feature of ES6
    const { contractRequests } = this.props;
    return (
      <div className="container">
        <h3>Moderator dashboard</h3>
        <div className="row">
          <div className="col s5">
            <UserList />
          </div>
          <div className="col s5 offset-s1">
            {/* ModeratorDashboard as a parent sends data to contractReqList */}
            <ContractRequestList contractRequests={contractRequests} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    // state.contractRequest -> rootReducer.contractRequest: contractRequestReducer И у него он берет запросы контрактов
    contractRequests: state.contractRequest.contractRequests
  };
};

// High order component - Другими словами декоратор, который соединяет redux и данный компонент
// Посылаем mapStateToProps чтобы redux понял что нужно вернуть в props
export default connect(mapStateToProps)(ModeratorDashboard);
