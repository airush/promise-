import React, { Component } from "react";
import "./ReviewContractAppeal.css";
import ContractRequestList from "../contractRequest/ContractRequestList";
import axios from "axios";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { deferContractAppealRequest } from "../../store/actions/moderContractRequestAction";
import LoadingCircle from "../auxiliary/LoadingCircle";
import authHeaders from "../../axiosConfig/authHeaders";

class ReviewContractAppeal extends Component {
  state = {};

  componentDidMount() {
    var elems = document.querySelectorAll(".dropdown-trigger");
    window.M.Dropdown.init(elems, {});
    this.fetchContractAppealsWithParams(null);
  }

  fetchContractAppealsWithParams = status => {
    console.log("Status Search ", status);
    authHeaders().then(headers => {
      axios
        .get("http://localhost:8080/api/contract_appeal/all", {
          params: {
            status: status
          }
        })
        .then(resp => {
          console.log("Success resp,", resp);
          this.setState({ contractAppealsSet: resp.data, active: status });
        })
        .catch(err => {
          console.log("Error: ", err);
        });
    });
  };
  render() {
    const contractAppealsSet = this.state.contractAppealsSet;

    // It is current contract Appeal and moderator should find pair for it
    const target = this.props.target;
    console.log(this.state, "STATE");
    return (
      <div className="container review-contract-appeals">
        {contractAppealsSet ? (
          <div className="row">
            <div className="col s2">
              <div className="collection">
                <a
                  onClick={() =>
                    this.fetchContractAppealsWithParams("UNCHECKED")
                  }
                  href="#!"
                  className={`collection-item ${this.state.active ===
                    "UNCHECKED" && "active"}`}
                >
                  Uncheked
                </a>
                <a
                  onClick={() => this.fetchContractAppealsWithParams("WAITING")}
                  href="#!"
                  className={`collection-item ${this.state.active ===
                    "WAITING" && "active"}`}
                >
                  Waiting
                </a>
                <a
                  onClick={() => this.fetchContractAppealsWithParams("SOLVED")}
                  href="#!"
                  className={`collection-item ${this.state.active ===
                    "SOLVED" && "active"}`}
                >
                  Done
                </a>
                <a
                  onClick={() => this.fetchContractAppealsWithParams(null)}
                  href="#!"
                  className={`collection-item ${this.state.active === null &&
                    "active"}`}
                >
                  All
                </a>
              </div>
            </div>
            <div className="col s7">
              <ContractRequestList data={this.state} />
            </div>
            <div className="col s3 ">
              {/* <!-- Dropdown Trigger --> */}
              <a
                style={{ width: "100%" }}
                className="dropdown-trigger btn"
                href="#!"
                data-target="dropdown1"
              >
                Search by tags
              </a>
              {/* <!-- Dropdown Structure --> */}
              <ul id="dropdown1" className="dropdown-content">
                <li>
                  <a href="#!">Tag1</a>
                </li>
                <li>
                  <a href="#!">Tag2</a>
                </li>
                <li className="divider" tabIndex="-1" />
                <li>
                  <a href="#!">Tag3</a>
                </li>
              </ul>
              <div className="input-field">
                <i className="material-icons prefix">search</i>
                <input
                  type="text"
                  id="autocomplete-input"
                  className="autocomplete"
                />
                <label htmlFor="autocomplete-input">search the best</label>
              </div>
              {target && (
                <div className="contract-appeal-target">
                  <div>
                    <h6>Current target</h6>

                    <i
                      onClick={() => {
                        this.props.deferContractAppealRequest(target);
                      }}
                      className="material-icons"
                    >
                      close
                    </i>
                    <div className="divider" />
                  </div>

                  <div>
                    <h6>
                      <Link to={"/contract/request/" + target.id}>
                        {target.title}
                      </Link>
                    </h6>

                    <p>
                      <span> Tags: </span>
                      {target.tagSet.map(o => o.name.toString()).join(",")}
                    </p>
                  </div>
                </div>
              )}
            </div>
          </div>
        ) : (
          <LoadingCircle></LoadingCircle>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    target: state.moderContractAppeal.contractAppealTarget
  };
};

const mapDispactToProps = dispatch => {
  return {
    deferContractAppealRequest: contractAppeal => {
      dispatch(deferContractAppealRequest(contractAppeal));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispactToProps
)(ReviewContractAppeal);
