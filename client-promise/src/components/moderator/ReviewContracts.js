import React, { Component } from "react";
import "./ReviewContractAppeal.css";
// import ContractRequestList from "../contractRequest/ContractRequestList";
import axios from "axios";
import { connect } from "react-redux";
// import { deferContractAppealRequest } from "../../store/actions/moderContractRequestAction";
import LoadingCircle from "../auxiliary/LoadingCircle";
import authHeaders from "../../axiosConfig/authHeaders";
import ContractList from "../contract/ContractList";

class ReviewContracts extends Component {
  state = {};
  componentDidMount() {
    authHeaders().then(headers => {
      axios
        .get("http://localhost:8080/api/moderator/contracts", { headers })
        .then(resp => {
          console.log("Success resp,", resp);

          this.setState({ contracts: resp.data });
        })
        .catch(err => {
          console.log("Error: ", err);
        });
    });
  }

  render() {
    return (
      <div className="container review-contract-appeals">
        {this.state.contracts ? (
          <div className="row">
            <div className="col s2">
              <div className="collection">
                <a href="#!" className="collection-item active">
                  Created contracts
                </a>
              </div>
            </div>
            <div className="col s7">
              {/* <ContractRequestList data={this.state} /> */}
              <ContractList data={this.state}></ContractList>
            </div>
            <div className="col s3 "></div>
          </div>
        ) : (
          <LoadingCircle></LoadingCircle>
        )}
      </div>
    );
  }
}

export default connect(
  null,
  null
)(ReviewContracts);
