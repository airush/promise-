import React from "react";
import UserListComponent from "./UserListComponent";

const UserList = () => {
  return (
    <div className="section">
      User List
      <UserListComponent />
      <UserListComponent />
    </div>
  );
};

export default UserList;
