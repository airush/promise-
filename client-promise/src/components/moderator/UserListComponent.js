import React from "react";
import { Link } from "react-router-dom";

const UserListComponent = () => {
  return (
    <div className="card z-depth-1">
      <div className="card-content indigo-text text-darken-3">
        <Link to="/users/1">
          <span className="card-title">Name</span>
        </Link>
        <p>Info</p>
        <p className="grey-text">Registered: 1st Nov, 2am</p>
      </div>
    </div>
  );
};

export default UserListComponent;
