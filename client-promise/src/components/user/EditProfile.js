import React, { Component } from "react";
import "./EditProfile.css";
import moment from "moment";
import authHeaders from "../../axiosConfig/authHeaders";
import axios from "axios";
import { connect } from "react-redux";
import LoadingCircle from "../auxiliary/LoadingCircle";
import { Helmet } from "react-helmet";
class EditProfile extends Component {
  state = {
    isSubmitDisavled: false
  };

  constructor(props) {
    super(props);
    this.state = { file: "", imagePreviewUrl: "" };
  }

  handleImageChange(e) {
    var file = e.target.files[0];

    const reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    };
    reader.readAsDataURL(file);
  }

  componentDidMount() {
    axios
      .get("http://localhost:8080/api/user/" + this.props.userId, {})
      .then(resp => {
        const user = resp.data;
        this.setState({
          user: true,
          status: user.status,
          firstName: user.firstName,
          lastName: user.lastName,
          gender: user.gender,
          about: user.about
        });

        var datepicker = document.querySelectorAll(".datepicker");
        window.M.Datepicker.init(datepicker, {
          yearRange: [1970, 2005],
          showClearBtn: true,
          showMonthAfterYear: true
        });

        window.$(".datepicker-done").click(() => {
          var datepickerValue = window.$("#birthday").val();

          this.setState({
            birthday: moment(datepickerValue, "MMM D, YYYY").toDate()
          });
        });
      })
      .catch(err => {
        console.log(err.response);
        console.log("Error: ", err);
      });
  }

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = e => {
    this.setState({ isSubmitDisavled: true });
    e.preventDefault();
    var file = this.state.file;

    var formdata = new FormData();

    formdata.append("avatar", file);

    const json = JSON.stringify(this.state);
    const blob = new Blob([json], {
      type: "application/json"
    });
    formdata.append("requestBody", blob);
    authHeaders().then(jwt => {
      axios
        .post("http://localhost:8080/api/user/edit", formdata, {
          headers: {
            Authorization: jwt.Authorization,
            "Content-Type": "multipart/form-data"
          }
        })
        .then(resp => {
          const updateData = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            avatarUrl: resp.data
          };
          this.props.updateAuthUserProfile(updateData);
          this.props.history.push("/user/" + this.props.userId);
        })
        .catch(err => {
          this.setState({ isSubmitDisavled: false });
          console.log(err.response);
          console.log("Error: ", err);
        });
    });
  };

  render() {
    // Нужно чтобы установить аватор после выбора фото
    const { user, status, firstName, lastName, gender, about } = this.state;

    const { imagePreviewUrl } = this.state;
    var $imagePreview = null;
    var imagePreviewBefore;
    if (imagePreviewUrl) {
      $imagePreview = <img src={imagePreviewUrl} alt=""></img>;
      imagePreviewBefore = "";
    } else {
      $imagePreview = <div className="previewText">200x200</div>;
      imagePreviewBefore = "image-preview-before";
    }

    return (
      <React.Fragment>
        <Helmet>
          <title>Edit profile</title>
        </Helmet>
        <div style={{ marginTop: 5 }}>
          {user ? (
            <div className="container">
              <div className="edit-profile row">
                <div className="col s3">
                  <div className="collection">
                    <a href="#!" className="collection-item active">
                      General
                    </a>
                  </div>
                </div>
                <div className="col s9">
                  <form
                    onSubmit={this.handleSubmit}
                    encType="multipart/form-data"
                  >
                    <h5>
                      <i className="material-icons">create</i>Edit profile
                    </h5>
                    <div className="divider"></div>

                    <div>
                      {/* Image block */}

                      <div
                        className={"image-preview " + imagePreviewBefore}
                        style={{ float: "right" }}
                      >
                        <div className=" center">{$imagePreview}</div>
                      </div>

                      <div className="file-field input-field">
                        <div className="btn-small">
                          <span>File</span>
                          <input
                            type="file"
                            name="avatar"
                            onChange={e => this.handleImageChange(e)}
                          />
                        </div>
                        <div
                          className="file-path-wrapper"
                          style={{ paddingRight: "7%" }}
                        >
                          <input
                            className="file-path validate"
                            type="text"
                            placeholder="Chouse the image"
                          />
                        </div>
                      </div>
                      {/* Start inputs */}
                      <div className="my-width-60-p input-field">
                        <input
                          placeholder="Status"
                          id="status"
                          onChange={this.handleChange}
                          type="text"
                          value={status ? status : ""}
                        />
                      </div>
                      <div className="my-width-60-p input-field">
                        <input
                          placeholder="First name"
                          id="firstName"
                          onChange={this.handleChange}
                          type="text"
                          value={firstName}
                        />
                      </div>
                      <div className=" input-field">
                        <input
                          placeholder="Last name"
                          id="lastName"
                          onChange={this.handleChange}
                          type="text"
                          value={lastName}
                        />
                      </div>
                      <div className="input-field">
                        <textarea
                          id="about"
                          onChange={this.handleChange}
                          className="materialize-textarea"
                          type="text"
                          placeholder="Type something about you"
                          value={about ? about : ""}
                        />
                      </div>

                      {/* Datepicker */}
                      <div className="right">
                        <i className="material-icons">date_range</i>
                        <input
                          id="birthday"
                          type="text"
                          className="datepicker my-width-60-p"
                          placeholder="Chouse birthday"
                        />
                      </div>
                      <div className="my-width-30-p">
                        <h6>Gender:</h6>
                        <p>
                          <label>
                            <input
                              className="with-gap"
                              id="gender"
                              name="gender"
                              type="radio"
                              value="MALE"
                              checked={gender && gender === "MALE"}
                              onChange={this.handleChange}
                            />
                            <span>Male</span>
                          </label>
                        </p>
                        <p>
                          <label>
                            <input
                              className="with-gap"
                              id="gender"
                              name="gender"
                              type="radio"
                              value="FEMALE"
                              checked={gender && gender === "FEMALE"}
                              onChange={this.handleChange}
                            />
                            <span>Female</span>
                          </label>
                        </p>
                      </div>
                    </div>

                    <button
                      disabled={this.state.isSubmitDisavled}
                      className=" right btn waves-effect waves-light"
                      style={{
                        margin: "0 15px 25px 0",
                        backgroundColor: "#5350EF"
                      }}
                    >
                      Submit
                      <i className="material-icons right">send</i>
                    </button>
                  </form>
                </div>
              </div>
            </div>
          ) : (
            <LoadingCircle></LoadingCircle>
          )}
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    userId: state.auth.userId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateAuthUserProfile: updateData => {
      dispatch({ type: "UPDATE_PROFILE", data: updateData });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfile);
