import React, { Component } from "react";
import "./UserProfile.css";
import axios from "axios";
import authHeaders from "../../axiosConfig/authHeaders";
import moment from "moment";
import LoadingCircle from "../auxiliary/LoadingCircle";
import { Helmet } from "react-helmet";
class UserProfile extends Component {
  state = {};

  componentDidMount() {
    const id = this.props.match.params.id;
    authHeaders().then(headers => {
      axios
        .get("http://localhost:8080/api/user/" + id, { headers })
        .then(resp => {
          this.setState({ user: resp.data });

          // Enable materialize js
          var tabs = document.querySelectorAll(".tabs");
          window.M.Tabs.init(tabs, {});

          var tooltips = document.querySelectorAll(".tooltipped");
          window.M.Tooltip.init(tooltips, {});
        })
        .catch(err => {
          console.log(err.response);
          console.log("Error: ", err);
        });
    });
  }

  render() {
    const user = this.state.user;
    return (
      <React.Fragment>
        <Helmet>
          <title>Profile {user ? user.username : ""}</title>
        </Helmet>
        <div className="container section" style={{ marginTop: 5 }}>
          {user ? (
            <div className="row">
              <div className="col s3">
                <div className="center-align user-profile-left-block  ">
                  <div>
                    <img
                      src={
                        user.avatarUrl
                          ? user.avatarUrl
                          : process.env.PUBLIC_URL + "/images/img_avatar.png"
                      }
                      alt=""
                    />
                  </div>
                  <h5>
                    {user.lastName} {user.firstName}
                  </h5>
                  <h6>
                    gender:{" "}
                    {user.gender ? user.gender.toLowerCase() : "not filled"}
                  </h6>
                  <h6>
                    Birthday:{" "}
                    {user.birthday
                      ? moment(user.birthday).format("ll")
                      : "not filled"}
                  </h6>
                </div>
              </div>
              <div className="user-profile col s9 lighten-3">
                {/* User profile header: Initials and last visited and When created at */}
                <div className="user-profile-header row">
                  <div className="col s7">
                    <h5>Profile: {user.username}</h5>
                    <h6>Status: {user.status}</h6>
                  </div>
                  <div className="col s5">
                    <p>Created at: {moment(user.createdAt).format("ll")}</p>
                    <p>Last visited: {moment(user.lastVisited).calendar()}</p>
                  </div>
                </div>
                {/* User details */}
                <div>
                  <div className="user-profile-details">
                    <p>About: {user.about ? user.about : "not filled yet"} </p>
                    {/* <h6>Hobbies: TODO</h6> */}
                  </div>
                  <div className="divider"></div>
                  {/* Badges */}
                  <div className="user-profile-badges">
                    <ul id="user-tabs-swipe" className="tabs">
                      <li className="tab col s3 active">
                        <a href="#user-tab-swipe-1">Badges</a>
                      </li>
                      <li className="tab col s3">
                        <a href="#user-tab-swipe-2">Achievements</a>
                      </li>
                    </ul>
                    <div id="user-tab-swipe-1" className=" col s12">
                      <i
                        className="material-icons tooltipped"
                        data-position="bottom"
                        data-tooltip="Learned golf"
                      >
                        golf_course
                      </i>
                      <i
                        className="material-icons tooltipped"
                        data-position="bottom"
                        data-tooltip="Top 1 User"
                      >
                        grade
                      </i>
                      <p>Coming soon</p>
                    </div>
                    <div id="user-tab-swipe-2" className="col s12">
                      Coming soon
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <LoadingCircle></LoadingCircle>
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default UserProfile;
