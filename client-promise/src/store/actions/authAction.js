import axios from "axios";

export const userLoginAction = userSignInDetails => {
  return dispatch => {
    dispatch({ type: "LOGIN_REQUEST" });
    // setTimeout to emulate fetching data
    setTimeout(() => {
      axios
        .post("http://localhost:8080/api/auth/login", userSignInDetails, {
          headers: { "Content-Type": "application/json" }
        })
        .then(resp => {
          const data = resp.data;
          localStorage.setItem("access-token", data.accessToken);
          localStorage.setItem("refresh-token", data.refreshToken);
          localStorage.setItem("expires-in", data.expiresIn);
          dispatch({ type: "LOGIN_SUCCESS", data });
        })
        .catch(err => {
          // if err.message = Network Error
          // if err.response.data.message = Error message from server
          const { message } = err.response ? err.response.data : err;
          dispatch({ type: "LOGIN_ERROR", message });
        });
    }, 100);
  };
};

export const userSignUpAction = userSignUpDetails => {
  return dispatch => {
    axios
      .post("http://localhost:8080/api/auth/signup", userSignUpDetails)
      .then(resp => {
        dispatch({ type: "SIGN_UP_SUCCESS", message: resp.data });
      })
      .catch(err => {
        dispatch({ type: "SIGN_UP_ERROR", message: err.response.data.message });
      });
  };
};

export const logoutAction = () => {
  return dispatch => {
    dispatch({ type: "LOGOUT" });
    // window.location.href = "http://localhost:3000/auth/login";
  };
};
