import SockJS from "sockjs-client";

var stompClient = null;
var chatUsername = null;
var localDispatch = null;

export const chatSetupConnection = usenname => {
  chatUsername = usenname;
  return dispatch => {
    console.log("Dispatching");
    // Need to dispatch request
    localDispatch = dispatch;

    // TODOD: Add authHeader
    const sockJS = new SockJS("http://localhost:8080/ws");
    stompClient = window.Stomp.over(sockJS);
    var headers = {
      "ws-username": usenname
    };
    stompClient.connect(headers, onConnected, onError);
  };
};

export const sendMessage = text => {
  return dispatch => {
    stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(text));
  };
};

function onMessageRecieved(text) {
  var response = JSON.parse(text.body);
  var message = {
    id: response.id,
    text: response.text,
    date: response.date,
    authorId: null,
    authorFullname: null
  };
  localDispatch({ type: "CHAT_MESSAGE_RECIEVED", message: message });
}

function onError() {
  console.log("Connection error..");
}

function onConnected() {
  console.log("Connecting to topic...");
  localDispatch({ type: "WEB_SOCKET_CONNECT_SUCCESS" });
  stompClient.subscribe("/topic/public", onMessageRecieved);

  stompClient.send(
    "/app/chat.addUser",
    {},
    JSON.stringify({ sender: chatUsername, type: "JOIN" })
  );
}
