import SockJS from "sockjs-client";

var stompClient = null;
var chatUsername = undefined;
var localDispatch = null;

export const contactChatSetupConnection = usenname => {
  chatUsername = usenname;
  return dispatch => {
    // Need to dispatch request
    localDispatch = dispatch;

    const sockJS = new SockJS("http://localhost:8080/ws");
    stompClient = window.Stomp.over(sockJS);
    var headers = {
      "ws-username": usenname
    };
    stompClient.connect(headers, onConnected, onError);
  };
};

export const sendMessage = message => {
  return dispatch => {
    stompClient.send(
      "/app/send",
      { "ws-username": chatUsername },
      JSON.stringify({ message: message.text, to: message.to })
    );
  };
};

function onMessageRecieved(text) {
  var response = JSON.parse(text.body);
  console.log("Coming parsed text", response);

  if (response.type === "NEW_MESSAGE") {
    const contact = response.contact;
    console.log("New message", contact);
    var contactId = contact.contactId;

    var message = {
      id: contact.id,
      text: contact.text,
      date: contact.date,
      userId: contact.userId,
      avatarUrl: contact.avatarUrl
    };
    localDispatch({
      type: "CONTACT_CHAT_MESSAGE_RESPONSE",
      data: {
        contactId,
        message
      }
    });
  } else if ((response.type = "NEW_CONTACT")) {
    console.log("NEW CONTACT", response.contact);
    localDispatch({
      type: "NEW_CONTACT",
      contact: response.contact
    });
  }
}

function onError() {
  console.log("Connection error..");
}

function onConnected() {
  localDispatch({ type: "WEB_SOCKET_CONNECT_SUCCESS" });
  stompClient.subscribe("/user/queue/reply", onMessageRecieved);
}
