import axios from "axios";
import authHeaders from "../../axiosConfig/authHeaders";

export const createContract = contract => {
  return dispatch => {
    console.log("contract", contract);
    const reqBody = {
      title: contract.title,
      contractDetails: contract.describe,
      charge: contract.charge,
      date: contract.approxLastDay,
      firstContractAppealId: contract.firstContractAppeal.id,
      secondContractAppealId: contract.secondContractAppeal.id,
      moderatorId: contract.moderatorId
    };
    // console.log(reqBody);
    authHeaders().then(headers => {
      axios
        .post(
          "http://localhost:8080/api/contract/create",
          reqBody,

          { headers }
        )
        .then(resp => {
          console.log("Resp", resp);
          // dispatch({ type: "CONTRACT_APPEAL_SUCCESS", resp });
        })
        .catch(err => {
          console.log("ERRoR", err.response);
          // dispatch({ type: "CONTRACT_APPEAL_ERROR", err });
        });
    });
  };
};
