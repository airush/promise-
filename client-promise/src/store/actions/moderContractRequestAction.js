export const connectToContractRequest = ({ contractAppeal }) => {
  return dispatch => {
    dispatch({
      // TODO : axios request to Set status INVESTIGATION
      type: "CONNECT_CONTRACT_APPEAL_REQUEST",
      data: { contractAppeal }
    });
  };
};

export const connectToContractRequestSecond = ({ contractAppeal }) => {
  return dispatch => {
    dispatch({
      // TODO : axios request to Set status INVESTIGATION
      type: "CONNECT_SECOND_CONTRACT_APPEAL_REQUEST",
      data: { contractAppeal }
    });
  };
};

export const deferContractAppealRequest = ({ contractAppeal }) => {
  return dispatch => {
    dispatch({ type: "DEFER_CONTRACT_APPEAL_REQUEST" });
  };
};
