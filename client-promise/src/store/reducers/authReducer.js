const initState = {
  userId: null,
  authError: false,
  username: null,
  initials: null,
  firstName: null,
  lastName: null,
  signUpSuccessMsg: null,
  isFetching: null,
  roles: [],
  avatarUrl: null
};

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case "LOGIN_REQUEST":
      return {
        ...state,
        isFetching: true,
        authError: false
      };
    case "LOGIN_ERROR":
      console.log("LOGIN_ERROR", action.message);
      return {
        ...state,
        authError: action.message,
        isFetching: false
      };
    case "LOGIN_SUCCESS":
      const {
        userId,
        username,
        firstName,
        lastName,
        roles,
        avatarUrl
      } = action.data;
      const initials = firstName.charAt(0) + lastName.charAt(0);
      console.log("LOGIN_SUCCESS");
      return {
        ...state,
        userId,
        username,
        initials,
        firstName,
        lastName,
        isFetching: false,
        authError: false,
        roles,
        avatarUrl
      };
    case "SIGN_UP_SUCCESS":
      console.log("SIGN_UP_SUCCESS");
      return {
        ...state,
        signUpSuccessMsg: action.message
      };
    case "SIGN_UP_ERROR":
      console.log("SIGN_UP_ERROR", action.message);
      return {
        ...state,
        authError: action.message
      };
    case "UPDATE_PROFILE":
      const avaUrl = action.data.avatarUrl;
      const lName = action.data.lastName;
      const fName = action.data.firstName;

      const init = fName.charAt(0) + lName.charAt(0);
      return {
        ...state,
        firstName: fName,
        lastName: lName,
        avatarUrl: avaUrl === "Null" ? state.avatarUrl : avaUrl,
        initials: init
      };
    default:
      return state;
  }
};

export default authReducer;
