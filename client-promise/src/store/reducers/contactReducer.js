const initState = {
  partnerUsername: undefined,
  fullname: undefined,
  messages: [],
  contacts: [],
  currentContact: undefined
};

const contractRequestReducer = (state = initState, action) => {
  switch (action.type) {
    case "SET_CONTACTS":
      const contacts = action.contacts;
      // const currentContact = contacts.length > 0 && contacts[0];
      return {
        ...state,
        contacts: contacts
        // currentContact
      };
    case "SET_CURRENT_CONTACT_MESSAGES":
      if (state.contacts.length) {
        const contact = state.contacts.find(
          contact => contact.id === action.id
        );
        const messages = contact.contactMessageSet;
        return {
          ...state,
          fullname: action.fullname,
          partnerUsername: contact.username,
          messages,
          currentContact: contact
        };
      }
      return state;
    case "CONTACT_CHAT_MESSAGE_RESPONSE":
      console.log("action", action);
      var contactIndex = state.contacts.findIndex(
        contact => contact.id === action.data.contactId
      );
      state.contacts[contactIndex].contactMessageSet.push(action.data.message);
      var newContacts2 = Object.assign([], state.contacts);
      var newMessages2 = Object.assign(
        [],
        newContacts2[contactIndex].contactMessageSet
      );
      return {
        ...state,
        messages: newMessages2,
        contacts: newContacts2
      };
    case "NEW_CONTACT":
      console.log("ACtion", action);
      return {
        ...state,
        contacts: [...state.contacts, action.contact]
      };
    default:
      return state;
  }
};

export default contractRequestReducer;
