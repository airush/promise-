const initState = {};

const contractRequestReducer = (state = initState, action) => {
  switch (action.type) {
    case "CREATE_CONTRACT_REQUEST":
      console.log("created contract", action.contractRequest);
      return initState;
    default:
      return state;
  }
};

export default contractRequestReducer;
