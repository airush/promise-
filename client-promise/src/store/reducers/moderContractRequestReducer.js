const initState = {
  contractAppealTarget: undefined,
  contractAppealTargetPair: undefined
};

const moderContractRequestReducer = (state = initState, action) => {
  switch (action.type) {
    case "CONNECT_CONTRACT_APPEAL_REQUEST":
      return {
        ...state,
        contractAppealTarget: action.data.contractAppeal
      };
    case "DEFER_CONTRACT_APPEAL_REQUEST":
      return {
        state: undefined
      };
    case "CONNECT_SECOND_CONTRACT_APPEAL_REQUEST":
      return {
        ...state,
        contractAppealTargetPair: action.data.contractAppeal
      };
    case "RESET_BOTH_CONTRACT_APPEAL_REQUESTS":
      return {
        contractAppealTarget: undefined,
        contractAppealTargetPair: undefined
      };
    default:
      return state;
  }
};

export default moderContractRequestReducer;
