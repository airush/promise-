import authReducer from "./authReducer";
import contractRequestReducer from "./contractRequestReducer";
import webSocketChatReducer from "./webSocketChatReducer";
import moderContractRequestReducer from "./moderContractRequestReducer";
import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import contactReducer from "./contactReducer";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["auth", "moderContractAppeal"]
};

const appReducer = combineReducers({
  auth: authReducer,
  contractRequest: contractRequestReducer,
  webSocketChat: webSocketChatReducer,
  moderContractAppeal: moderContractRequestReducer,
  contactReducer: contactReducer
});

const rootReducer = (state, action) => {
  if (action.type === "LOGOUT") {
    console.log("RESET DATA");
    storage.removeItem("persist:root");
    state = undefined;
  }

  return appReducer(state, action);
};
export default persistReducer(persistConfig, rootReducer);
