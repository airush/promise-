const initState = {
  connectionMessage: null,
  messages: []
};

const webSocketChatReducer = (state = initState, action) => {
  switch (action.type) {
    case "WEB_SOCKET_CONNECT_SUCCESS":
      return {
        ...state,
        connectionMessage: "Connented successfully!"
      };
    case "CHAT_MESSAGE_RECIEVED":
      return {
        ...state,
        messages: [...state.messages, action.message]
      };
    default:
      return state;
  }
};

export default webSocketChatReducer;
